<?php
require __DIR__ . '/__connect_db.php';
$pname = 'secondhand';


?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="css/reset.css">
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>
	<link rel="stylesheet" type="text/css" href="slick/slick-theme.css"/>
	<link rel="stylesheet" type="text/css" href="css/second.css">
	<link rel="stylesheet" type="text/css" href="slick/slick.css"/>
 <?php include __DIR__. "/__page_head.php" ?>
	<title>second</title>

<body>
	<?php include __DIR__. '/__page_header.php' ?>
	<div class="second_choose">
	<input class="choose_pro"  type="text" placeholder="最低價格" >_
	<input class="choose_pro"  type="text" placeholder="最高價格" >
	<input class="choose_pro"  type="text" placeholder="作品分類" >
	<input class="choose_pro"  type="text" placeholder="商品狀況" >
	<input class="choose_pro"  type="text" placeholder="近期熱門" >
	<a class="second_search" href="">搜尋</a>
	</div>
<div class="secone_new">最新上架</div>
		<ul class="second_pic">
			<a href="__page_product_detail.php"><li class="second">
				<img class="sccond_imgshow" src="css/images/second/1-1.png">
				<div class="second_sale">
					<p class="second_neme">魔法少女<span class="nt"> NT 1750</span></p>
					<p class="second_na">鹿目圓</p>
				</div>
			</li></a>		
			<li class="second">
				<img class="sccond_imgshow" src="css/images/second/1.png">
				<div class="second_sale">
					<p class="second_neme">進擊的巨人  <span class="nt">NT 1750</span></p>
					<p class="second_na">里維</p>
				</div>	
			</li>
			<li class="second">
				<img class="sccond_imgshow" src="css/images/second/1-0.png">
				<div class="second_sale">
					<p class="second_neme">一拳超人  <span class="nt">NT 1198</span></p>
					<p class="second_na">戰慄的龍捲</p>
				</div>
			</li>
			<li class="second">		
				<img class="sccond_imgshow" src="css/images/second/1-3.png">
				<div class="second_sale">
					<p class="second_neme">一拳超人  <span class="nt">NT 1198</span></p>
					<p class="second_na">傑諾斯</p>
				</div>
			</li>
		</ul>

		<div class="secone_new">保存良好</div>
			<ul class="second_pic">
				<li class="second">
					<img class="sccond_imgshow" src="css/images/second/2.png">
					<div class="second_sale">
						<p class="second_neme">艦隊  <span class="nt">NT 1448</span></p>
						<p class="second_na">睦月改二</p>
					</div>
				</li>
				<li class="second">
					<img class="sccond_imgshow" src="css/images/second/2-1.png">
					<div class="second_sale">
						<p class="second_neme">碧藍幻想  <span class="nt">NT 1224</span></p>
						<p class="second_na">露莉亞</p>
					</div>				
				</li>
				<li class="second">
					<img class="sccond_imgshow" src="css/images/second/2-2.png">
					<div class="second_sale">
						<p class="second_neme">甲鐵城   <span class="nt">NT 1198</span></p>
						<p class="second_na">無名</p>
					</div>
				</li>
				<li class="second">
					<img class="sccond_imgshow" src="css/images/second/2-3.png">
					<div class="second_sale">
						<p class="second_neme">神奇的未來  <span class="nt">NT 1049</span></p>
						<p class="second_na">初音未來</p>
					</div>
				</li>
			</ul>
		<div class="secone_new">價格最高</div>
		<ul class="second_pic">
			<li class="second">	
				<img class="sccond_imgshow" src="css/images/second/3.png">
				<div class="second_sale">
					<p class="second_neme">魔法少女小圓  <span class="nt">NT 2198</span></p>
					<p class="second_na">巴麻美</p>
				</div>
			</li>
			<li class="second">
				<img class="sccond_imgshow" src="css/images/second/3-1.png">
				<div class="second_sale">
					<p class="second_neme">魔法少女小圓  <span class="nt">NT 2124</span></p>
					<p class="second_na">曉美焰</p>
				</div>			
			</li>
			<li class="second">
				<img class="sccond_imgshow" src="css/images/second/3-2.png">
				<div class="second_sale">
					<p class="second_neme">庫洛魔法使<span class="nt">NT 2224</span></p>
					<p class="second_na">李小狼</p>
				</div>
			</li>
			<li class="second">
				<img class="sccond_imgshow" src="css/images/second/3-3.png">
				<div class="second_sale">
					<p class="second_neme">為美好的世界獻上祝福！  <span class="nt">NT 2124</span></p>
					<p class="second_na">達克妮絲</p>
				</div>
			</li>
		</ul>

		<div class="secone_new ">價格最低</div>
		<ul class="second_pic slow">
			<li class="second">
				<img class="sccond_imgshow" src="css/images/second/4.png">
				<div class="second_sale">
					<p class="second_neme">Grand Order<span class="nt">NT 1249</span></p>
					<p class="second_na">Saber</p>
				</div>
			</li>
			<li class="second">
				<img class="sccond_imgshow" src="css/images/second/4-1.png">
				<div class="second_sale">
					<p class="second_neme">動物朋友<span class="nt">NT 1124</span></p>
					<p class="second_na">籔貓</p>
				</div>				
			</li>
			<li class="second">
				<img class="sccond_imgshow" src="css/images/second/4-2.png">
				<div class="second_sale">
					<p class="second_neme">刀劍神域<span class="nt">NT 1374</span></p>
					<p class="second_na">桐人</p>
				</div>
			</li>
			<li class="second">
				<img class="sccond_imgshow" src="css/images/second/4-3.png">
				<div class="second_sale">
					<p class="second_neme">刀劍神域<span class="nt">NT 1049</span></p>
					<p class="second_na">詩乃</p>
				</div>
			</li>
		</ul>
		
    <?php include __DIR__. '/__page_foot.php' ?>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript" src="slick/slick.min.js"></script>
<script src="http://cdn.tutorialzine.com/misc/enhance/v3.js" async></script>

<script type="text/javascript">
	// $(document).ready(function(){
	//   $('.second_pic').slick({
	//    dots: true,
	//    infinite: true,
 //  	   slidesToShow: 3,
 // 	   slidesToScroll: 1,
	//   });
	// });
</script>
</body>

</html>