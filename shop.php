<?php
require __DIR__ . '/__connect_db.php';
$pname = 'shop';


?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="css/reset.css">
	<link rel="stylesheet" type="text/css" href="css/shoper.css">
 <?php include __DIR__. "/__page_head.php" ?>
	<title>shop</title>
<body>
<?php include __DIR__. '/__page_header.php' ?>
	<!-- 店鋪 -->
	<div class="shop_width">
		<div class="shop_circle">
			<div class="shop_title">店鋪資訊</div>
		</div>
	</div>
	<div class="shop_pic">
		<ul >
			<li class="shopa">
				<div class="img_map">
					<img class="imgshow" src="css/images/shopa/shop_img1.png">
					<div class="img_overlay">
						<img class="imgshow" src="css/images/shopa/map01.png">
					</div>
				</div>
				<div class="opacity">
					<p class="address">0900-2130</p>
					<p class="address">周日公休</p>
					<p class="address">02-2567-1557#2</p>
					<p class="address">106台北市大安區敦化南路一段187巷52號</p>
				</div></li>
			</ul>
		</ul>
		<ul>
			<li class="shopa">
				<div class="img_map">
					<img class="imgshow"  src="css/images/shopa/shop_img2.png">
					<div class="img_overlay">
						<img class="imgshow" src="css/images/shopa/map02.png">
					</div>
				</div>
				<div class="opacity">
					<p class="address">0900-2130</p>
					<p class="address">周日公休</p>
					<p class="address">02-2331-5666</p>
					<p class="address">108台北市萬華區西寧南路70號</p>
				</div></li>
			</ul>
			<ul>
				<li class="shopa">
					<div class="img_map">
						<img class="imgshow"  src="css/images/shopa/shop_img3.png">
						<div class="img_overlay">
							<img  class="imgshow" src="css/images/shopa/map03.png">
						</div>
					</div>
					<div class="opacity">
						<p class="address">0900-2130</p>
						<p class="address">周日公休</p>
						<p class="address">02-2558-6461</p>
						<p class="address">100台北市中正區市民大道100號</p>
					</div>
				</li>
			</ul>
			
		</div>
		<!-- 其他訊息 -->
		<div class="shop_width">
			<div class="shop_circle">
				<div class="shop_title">店鋪消息</div>
			</div>
		</div>
		<div class="store_banner">
				<ul class="shop_News_title">
					<li><a href="#tab1" class="shop_News">門市活動</a></li>
					<li><a href="#tab2" class="shop_News">全新開幕</a></li>
					</ul><!-- 切換的頁面 -->
				<div class="shop_News_content">
					<div id="tab1" class="content">
						<ul class="tab_container">
							<li><img src="css/images/shopa/newopen.png"></li>
							<li class="newopen_text">
								<p class="fontsize open_time">2017/06/25</p>
								<p class="fontsize open_big">7/1全新開幕</p>
								<p class="fontsize open_content">Fantaroid  終於  終於  終於 要在7/1正式與大家見面了 經過了長時間的籌備 了解要成就一件事 真的不容易 感恩所有喜愛的顧客支持我們!</p>
							</li>
						</ul>
						</div><!-- tab1 -->
						<div id="tab2" class="content">
							<ul class="tab_container">
								<li><img src="css/images/shopa/newopen0.png"></li>
								<li class="newopen_text">
									<p class="fontsize open_time">2017/07/01</p>
									<p class="fontsize open_big">Fantaroid熱身賽 好禮大放送！</p>
									<p class="fontsize open_content">好禮大放送!好禮大放送!7/8起Fantariod自辦熱身賽好禮大放送，將海賊王等人可愛公仔，於8月底，將好禮送贈與第一名，</p>
								</li>
							</ul>
							</div><!-- tab2 -->
							</div><!-- shop_News_content -->
							</div><!-- activepage -->
							</div><!-- widthcenter -->
							<div class="shop_width">
								<div class="shop_circle">
									<div class="shop_title">相關訊息</div>
								</div>
							</div>
							<ul class="widthcenter">
								<li class="cont cont_pic1">
									<p class="cont_text">新品上市</p>
									<img src="css/images/shopa/0.png">
									<div class="opacity">
									<p class="cont_title">Triad Primus</p>
									<p class="cont_title_cont">加蓮、奈緒、凜三人所組成登場！</p></div>
								</li>
								<li class="cont cont_pic1">
									<p class="cont_text">動漫活動</p>
									<img src="css/images/shopa/01.png">
									<div class="opacity">
									<p class="cont_title">台中國際動漫</p>
									<p class="cont_title_cont"> 8月11日至9月24日，台中文創園區雅堂館!</p></div>
								</li>
								<li class="cont cont_pic1"><p class="cont_text">人才招募</p>
									<img src="css/images/shopa/shop3.png">
									<div class="opacity">
									<p class="cont_title">找的就是你!</p>
									<p class="cont_title_cont">若你也是個喜愛二頭身，有......</p><div>
									</li>
								</ul>	
							</body>
 <?php include __DIR__. '/__page_foot.php' ?>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="http://cdn.tutorialzine.com/misc/enhance/v3.js" async></script>
<script
  src="https://code.jquery.com/jquery-3.2.1.js"
  integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
  crossorigin="anonymous">
  </script>

<script type="text/javascript">
var $defaultLi = $('ul.shop_News_title li').eq(0).addClass('active');
	$($defaultLi.find('a').attr('href')).siblings().hide();
//點擊按鈕時
$('.shop_News_title li').click(function(){
	// 找出li中超聯結href(#id)
	console.log($(this).index())
	var $thistab = $(this)
	var clickTab = $thistab.find('a').attr('href');
	// 點擊到的li頁籤加上.active
	// 兄弟元素中有.active的都移除class
	$('.shop_News_title li').removeClass('active');
	$thistab.addClass('active')
	// 但入相對應的內容，並隱藏兄弟元素
	$(clickTab).stop(false,true).fadeIn().siblings().hide();

	return false;
}).find('a').focus(function(){
	this.blur();	
});

</script>
</body>

</html>