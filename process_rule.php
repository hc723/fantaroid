<?php
require __DIR__ . '/__connect_db.php';
$pname = 'process_rule';


?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta hprop-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="css/reset.css">
	<link rel="stylesheet" type="text/css" href="css/process_rule.css">
	<?php include __DIR__. "/__page_head.php" ?>
</head>
<body>
	<?php include __DIR__ . '/__page_header.php' ?>
	<div class="title">
		<h1 class="all_rule">二手販賣同意書條款</h1>
	</div>
	<div class="all">
		<p class="one">	
			<span class="title_one"><span class="font_one">1.</span>二手賣家身份需是</span><br>
			<span class="title_content">1.持中華民國身分證</span><br>
			<span class="title_content">2.持中華民國居留證，且居住在台灣的外籍人</span><br>
		</p>
		<p class="two">	
			<span class="title_one"><span class="font_one">2.</span>收購二手商品範疇</span><br>
			<span class="title_content">1. 若賣家有所疑慮，不清楚收購二手商品之範疇，賣家可至「二手專區」 →「二手商品販賣流程」查看。	</span><br>
			<span class="title_content">2. 相同商品，我們僅提供每位賣家上架3個。<br>*我們不收購的二手商品如下：<br>(1)	配件不齊全<br>(2)	有瑕疵、水漬、殘破者、污損、塗鴉、膠貼、蟲穢、異味之商品<br>(3)	商品與上傳照片不符者<br>若我們驗收到上述情形之商品，我們將會執行本服務條款。<br></span>
		</p>
		<p class="three">
			<span class="title_one"><span class="font_one">3.</span>商品收購價</span><br>
			<span class="title_content">1.二手商品「收購價」由我們與賣家評估和協調，達成共識之後，會請賣家將商品帶至店鋪，收購後會請賣家簽名。</span><br>
			<span class="title_content">2.商品收購之後，賣家不得以其他理由要求原價買回。<br>
			<br></span>
		</p>
		<p class="four">
			<span class="title_one"><span class="font_one">4.</span>我們保留彈性調整二手商品收購服務條款之權利，調整內容即公佈於網頁，不另行通知賣家。<br></span>
		</p>
	</div>	
	<div class="process_buttom">
		<a class="process_back" href="secondhand.php">回到二手商品頁</a>
		<a class="process_willsale" href="process_rule.php">同意</a>
	</div>
	


<?php include __DIR__. "/__page_foot.php" ?>
	</body>
</html>