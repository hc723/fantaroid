<?php
require __DIR__ . '/__connect_db.php';
$pname = 'history';

if(!isset($_SESSION['user'])){
    exit;
}

$t = time() - 180*24*60*60;
$t2 = date('Y-m-d', $t);

$sql = sprintf("SELECT * FROM `orders` WHERE `member_sid`=%s AND `order_date` > '%s' ORDER BY `order_date` DESC",
        $_SESSION['user']['id'],
        $t2
    );
$rs = $mysqli->query($sql);

//$o_sids = array();
while($r = $rs->fetch_assoc()){
    $orders[] = $r;
    $o_sids[] = $r['sid'];
}

//print_r($orders);
//print_r($o_sids);
//exit;

// 訂單明細
$sql = sprintf("SELECT
  d.`order_sid`, d.`product_sid`, d.`price`, d.`quantity`,
  p.`bookname`
FROM `order_details` d
JOIN `products` p ON d.product_sid=p.sid
WHERE `order_sid` IN (%s);",
    implode(',', $o_sids)
    );
$rs = $mysqli->query($sql);
while($r = $rs->fetch_assoc()){
    $details[] = $r;
}
//print_r($details);
//exit;

?>
<?php include __DIR__. '/__page_head.php' ?>

<div class="container">
    <?php include __DIR__. '/__navbar.php' ?>


        <div class="col-md-12">

            <table class="table table-striped">
                <thead>
                <tr>
                    <th>訂單編號</th>
                    <th>總價</th>
                    <th>訂購時間</th>
                    <th>細項</th>
                </tr>
                </thead>
                <tbody>

                <?php foreach($orders as $o_item): ?>
                <tr>
                    <th><?= $o_item['sid'] ?></th>
                    <td><?= $o_item['amount'] ?></td>
                    <td><?= $o_item['order_date'] ?></td>
                    <td>
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>商品名稱</th>
                                <th>價格</th>
                                <th>數量</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php foreach($details as $d):
                                    if($d['order_sid']==$o_item['sid']):
                                    ?>
                                    <tr>
                                        <td><?= $d['bookname'] ?></td>
                                        <td><?= $d['price'] ?></td>
                                        <td><?= $d['quantity'] ?></td>
                                    </tr>
                                <?php
                                    endif;
                                endforeach;
                                ?>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <?php endforeach; ?>
                </tbody>
            </table>


        </div>



</div>
    <script>


    </script>
<?php include __DIR__. '/__page_foot.php' ?>