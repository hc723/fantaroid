<?php
require __DIR__ . '/__connect_db.php';
$pname = 'index';


?>
<?php include __DIR__. '/__page_head.php' ?>
<link type="text/css" rel="stylesheet" href="slick/slick/slick.css">
<link type="text/css" rel="stylesheet" href="slick/slick/slick-theme.css">
<link rel="stylesheet" type="text/css" href="css/indexwen.css">

<style>
</style>

<div class="container">
    <?php include __DIR__. '/__page_header.php' ?>
    <div class="ani_frame">
        <div class="perspective">
            <div class="ani_logo">
                <div class="mask fa"></div>
                <div class="mask nt"></div>
                <div class="mask a"></div>
                <div class="mask r"></div>
                <div class="o_pers">
                <div class="o"></div>
                </div>
                <div class="mask i"></div>
                <div class="mask d"></div>
            </div>
        </div>
    </div><!--ani_frame-->

    <div class="main">
        <div class="content">
            <div class="slider">
                <img src="css/images/index/slider1.png" alt="">
                <img src="css/images/index/slider2.png" alt="">
                <img src="css/images/index/slider3.png" alt="">
            </div><!--slider-->
    <div class="index">
        
    </div>
 <div class="secone_new">商品情報</div>
    <ul class="second_pic botton">
     <a href="single-product.php?sid=11">
            <li class="second">
            <img class="sccond_imgshow" src="css/images/index/01.png">
            <div class="second_sale">
                <p class="second_neme">名偵探柯南<span class="nt"> NT 1162</span></p>
                <p class="second_na">江戶川柯南</p>
            </div>
            </li>
        </a>
        <a href="single-product.php?sid=17">
            <li class="second">
            <img class="sccond_imgshow" src="css/images/index/1.png">
            <div class="second_sale">
                <p class="second_neme">斬首循環<span class="nt"> NT 1339</span></p>
                <p class="second_na">玖渚友</p>
            </div>
            </li>
        </a>
        <a href="single-product.php?sid=18">       
            <li class="second">
                <img class="sccond_imgshow" src="css/images/index/2.png">
                <div class="second_sale">
                    <p class="second_neme">排球少年!! <span class="nt">NT 1112</span></p>
                    <p class="second_na">灰羽列夫</p>
                </div>  
            </li>
        </a>
        <a href="single-product.php?sid=19">
            <li class="second">
                <img class="sccond_imgshow" src="css/images/index/3.png">
                <div class="second_sale">
                    <p class="second_neme">排球少年!!  <span class="nt">NT 1061</span></p>
                    <p class="second_na">夜久衛輔</p>
                </div>
            </li>
        </a>
    </ul>

    <ul class="second_pic ">
        <a href="single-product.php?sid=20">
            <li class="second ">
                <img class="sccond_imgshow" src="css/images/index/4.png">
                <div class="second_sale">
                    <p class="second_neme">小愛<span class="nt">NT 1213</span></p>
                    <p class="second_na">星期一的豐滿</p>
                </div>
            </li>
        </a>
        <a href="single-product.php?sid=21">
            <li class="second">
                <img class="sccond_imgshow" src="css/images/index/5.png">
                <div class="second_sale">
                    <p class="second_neme">櫻花任務  <span class="nt">NT 1213</span></p>
                    <p class="second_na">木春由乃</p>
                </div>              
            </li>
        </a>
        <a href="single-product.php?sid=22">
            <li class="second">
                <img class="sccond_imgshow" src="css/images/index/6.png">
                <div class="second_sale">
                    <p class="second_neme">排球少年!!<span class="nt">NT 1112</span></p>
                    <p class="second_na">牛島若利</p>
                </div>
            </li>
        </a>
        <a href="single-product.php?sid=23">
            <li class="second">
                <img class="sccond_imgshow" src="css/images/index/7.png">
                <div class="second_sale">
                    <p class="second_neme">少女與戰車 <span class="nt">NT 1213</span></p>
                    <p class="second_na">五十鈴華</p>
                </div>
            </li>
        </a>
    </ul>
    <div class="secone_new">二手商品</div>
            <ul class="second_pic botton">
                <a href="single-product.php?sid=24">
                    <li class="second"> 
                        <img class="sccond_imgshow" src="css/images/index/8.png">
                        <div class="second_sale">
                            <p class="second_neme">少女與戰車<span class="nt">NT 1213</span></p>
                            <p class="second_na">秋山優花里</p>
                        </div>
                    </li>
                </a>
                <a href="single-product.php?sid=25">
                    <li class="second">
                        <img class="sccond_imgshow" src="css/images/index/9.png">
                        <div class="second_sale">
                            <p class="second_neme">小魔女學園  <span class="nt">NT 1137</span></p>
                            <p class="second_na">亞可·卡嘉莉</p>
                        </div>          
                    </li>
                </a>
                <a href="single-product.php?sid=26">
                    <li class="second">
                        <img class="sccond_imgshow" src="css/images/index/10.png">
                        <div class="second_sale">
                            <p class="second_neme">少女與戰車  <span class="nt">NT 1213</span></p>
                            <p class="second_na"> 冷泉麻子</p>
                        </div>
                    </li>
                </a>
                <a href="single-product.php?sid=27">
                    <li class="second">
                        <img class="sccond_imgshow" src="css/images/index/11.png">
                        <div class="second_sale">
                            <p class="second_neme">BML 2016 Ver. <span class="nt">NT 1137</span></p>
                            <p class="second_na">33娘</p>
                        </div>
                    </li>
                </a>
            </ul>

            <ul class="second_pic last">
                <a href="single-product.php?sid=28">
                    <li class="second">
                        <img class="sccond_imgshow" src="css/images/index/12.png">
                        <div class="second_sale">
                            <p class="second_neme">LoveLive! Sunshine!!<span class="nt">NT 884</span></p>
                            <p class="second_na"> 黑澤黛雅</p>
                        </div>
                    </li>
                </a>
                <a href="single-product.php?sid=29">
                    <li class="second">
                        <img class="sccond_imgshow" src="css/images/index/13.png">
                        <div class="second_sale">
                            <p class="second_neme">LoveLive!<span class="nt">NT 1213</span></p>
                            <p class="second_na">Lancer/斯卡哈</p>
                        </div>              
                    </li>
                </a>
                <a href="single-product.php?sid=30">
                    <li class="second">
                        <img class="sccond_imgshow" src="css/images/index/14.png">
                        <div class="second_sale">
                            <p class="second_neme">為美好的世界獻上祝福！2<span class="nt">NT 1137</span></p>
                            <p class="second_na">惠惠</p>
                        </div>
                    </li>
                </a>
                <a href="single-product.php?sid=31">
                    <li class="second">
                        <img class="sccond_imgshow" src="css/images/index/15.png">
                        <div class="second_sale">
                            <p class="second_neme">排球少年!!<span class="nt">NT 1162</span></p>
                            <p class="second_na">赤葦京治</p>
                        </div>
                    </li>
                </a>
                </ul>
        <div class="secone_new">最新預購</div>
            <ul class="second_pic botton">
                <a href="single-product.php?sid=32">
                    <li class="second"> 
                        <img class="sccond_imgshow" src="css/images/index/16.png">
                        <div class="second_sale">
                            <p class="second_neme">YURI!!!on ICE<span class="nt">NT 1137</span></p>
                            <p class="second_na">維克多・尼基弗洛夫</p>
                        </div>
                    </li>
                </a>
                <a href="single-product.php?sid=33">
                    <li class="second">
                        <img class="sccond_imgshow" src="css/images/index/17.png">
                        <div class="second_sale">
                            <p class="second_neme">BanG Dream!  <span class="nt">NT 1137</span></p>
                            <p class="second_na">戶山香澄</p>
                        </div>          
                    </li>
                </a>
                <a href="single-product.php?sid=34">
                    <li class="second">
                        <img class="sccond_imgshow" src="css/images/index/18.png">
                        <div class="second_sale">
                            <p class="second_neme">林克 荒野之息  <span class="nt">NT 1466</span></p>
                            <p class="second_na"> 薩爾達傳說
</p>
                        </div>
                    </li>
                </a>
                <a href="single-product.php?sid=35">
                    <li class="second">
                        <img class="sccond_imgshow" src="css/images/index/19.png">
                        <div class="second_sale">
                            <p class="second_neme">刀劍亂舞-ONLINE  <span class="nt">NT 884</span></p>
                            <p class="second_na">
三日月宗近 真劍必殺裝</p>
                        </div>
                    </li>
                </a>
            </ul>

            <ul class="second_pic last">
                <a href="single-product.php?sid=36">
                    <li class="second">
                        <img class="sccond_imgshow" src="css/images/index/20.png">
                        <div class="second_sale">
                            <p class="second_neme">艦隊Collection
<span class="nt">NT 1466</span></p>
                            <p class="second_na"> Pola</p>
                        </div>
                    </li>
                </a>
                <a href="single-product.php?sid=37">
                    <li class="second">
                        <img class="sccond_imgshow" src="css/images/index/21.png">
                        <div class="second_sale">
                            <p class="second_neme">艦隊Collection
<span class="nt">NT 1794</span></p>
                            <p class="second_na">厭戰號</p>
                        </div>              
                    </li>
                </a>
                <a href="single-product.php?sid=38">
                    <li class="second">
                        <img class="sccond_imgshow" src="css/images/index/22.png">
                        <div class="second_sale">
                            <p class="second_neme">艦隊Collection<span class="nt">NT 1263</span></p>
                            <p class="second_na">吹雪改二</p>
                        </div>
                    </li>
                </a>
                <a href="single-product.php?sid=39">
                    <li class="second">
                        <img class="sccond_imgshow" src="css/images/index/23.png">
                        <div class="second_sale">
                            <p class="second_neme">艦隊Collection<span class="nt">NT 1263</span></p>
                            <p class="second_na">瑞鶴改</p>
                        </div>
                    </li>
                </a>
                </ul>

<button type="button" class="back-to-top">
    <span class="label">TOP</span>
</button>
     
            

<script type="text/javascript" src="script/jquery-migrate-1.2.1.js"></script>
<script type="text/javascript" src="slick/slick/slick.js"></script>
<script>
$(function () {

    /*
     * Slideshow
     */
    // 對每個有slideshow類別的元素進行處理
    $('.slider').each(function () {

        var $slides = $(this).find('img'), // 所有Slide
            slideCount = $slides.length,   // Slide個數
            currentIndex = 0;              // 目前Slide的index
        // 淡入顯示首張Slide
        $slides.eq(currentIndex).fadeIn();

        // 每7500毫秒就執行showNextSlide函數
        setInterval(showNextSlide, 2500);

          // 顯示下一張Slide的函數
        function showNextSlide () {

                        //下張Slide的index
                        //(如果是最後一張Slide，則會到第一張)
            var nextIndex = (currentIndex + 1) % slideCount;

            // 目前的Slide淡出顯示
            $slides.eq(currentIndex).fadeOut();

            // 下一張Slide淡入顯示
            $slides.eq(nextIndex).fadeIn();

               // 更新目前的index
            currentIndex = nextIndex;
        }

    });
    $scrollTop=$("body").scrollTop();
      $(window).scroll(function() {
        if ( $(this).scrollTop() > 400){
            $('.back-to-top').fadeIn("fast");
        } else {
            $('.back-to-top').stop().fadeOut("fast");
        }
      });
     
     /*
     * Back-toTop button (Smooth scroll)
     */
    $('.back-to-top').each(function () {

        
        // 檢測html、body何者為可捲動元素
        var $el = $(scrollableElement('html', 'body'));

        // 設定按鈕的點擊事件
        $(this).on('click', function (event) {
            event.preventDefault();
            $el.animate({ scrollTop: 0 }, 1000);
        });
    });

    // 透過scrollTop檢測可用元素的函數
    // http://www.learningjquery.com/2007/10/improved-animated-scrolling-script-for-same-page-links#update4
    function scrollableElement (elements) {
        var i, len, el, $el, scrollable;
        for (i = 0, len = arguments.length; i < len; i++) {
            el = arguments[i],
            $el = $(el);
            if ($el.scrollTop() > 0) {
                return el;
            } else {
                $el.scrollTop(1);
                scrollable = $el.scrollTop() > 0;
                $el.scrollTop(0);
                if (scrollable) {
                    return el;
                }
            }
        }
        return [];
    }
});


   
</script>
<?php include __DIR__. '/__page_foot.php' ?>