<?php
require __DIR__ . '/__connect_db.php';
$pname = 'cart_list';

if(empty($_SESSION['cart'])){
    $has_data = false;
} else {
    $keys = array_keys($_SESSION['cart']);

    $sql = sprintf("SELECT * FROM `products` WHERE `sid` IN (%s)", implode(',', $keys));

    $rs = $mysqli->query($sql);

    while($row = $rs->fetch_assoc()){
        $row['qty'] = $_SESSION['cart'][$row['sid']]; // 取得某項商品的數量
        $c_prod[ $row['sid'] ] = $row;
    }
    $has_data = true;
//print_r($c_prod);
//exit;
}

?>
<style>
    body{
        padding: 0;
        margin: 0;
        box-sizing: border-box;
        background: #FBF9F5;
        font-family:"微軟正黑體";
    }
    /*跳出訊息視窗的設定*/
    .alert{
        position: absolute;
        left: 50%;
        z-index: 1;
        margin-left: -100px;
        width: 200px;
        height: 50px;
        text-align: center;
        line-height: 50px;
        font-size: 20px;
        color: #fff;
        background-color: rgba(251,129,52,.8); 
    }
    .container{
        min-height: 80vh;
    }
    /*外面的盒子*/
    .cart_listbox{
        max-width: 960px;
        margin: 0 auto;
        padding: 30px;
    }
    .text_pic{
        line-height: 40px;
        width: 1000px;
        padding: 50px;
        border:1px solid #ccc;
        font-size: 20px;
    }
    .color{
        background: #FB9134;
        color:#fff;
    }
    /*.text_pic指定下面的td*/
    .text_pic td{
        width: 160px;
        vertical-align: middle;
        text-align: center;
        padding: 30px 0;
    }
    .totle{
        width: 955px;
        text-align: right;
        padding: 10px;
    }
    .ok{
        
    }
    a.btn{
        color:#fff;
        background: #FB8134;
        padding: 5px;
    }
    .imgsrc{
        width: 165px;   
    }
    td.del{
        /*background: #FB8134;*/
        color: #fff;
    }
    .smail{
        width: 100px;
        font-size: 20px;
        height: 40px;
        background: #345258;
        margin: 0 auto;
        cursor: pointer;

    }
</style>
<?php include __DIR__. '/__page_head.php' ?>

<div class="container">
    <?php include __DIR__. '/__page_header.php' ?>

    <div class="cart_listbox">
        <?php if($has_data): ?>
        <table class="table table-striped">
            <thead>
            <tr class="text_pic color">
                <th class="picflex">圖片</th>
                <th class="picflex">產品名稱</th>
                <th class="picflex">價格</th>
                <th class="picflex">數量</th>
                <th class="picflex">小計</th>
                <th class="picflex">移除</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($c_prod as $item): ?>
            <tr class="text_pic" 
            data-sid="<?= $item['sid'] ?>">
                <td><img class="imgsrc" src="<?= $item['img_id'] ?>" alt="<?= $item['name'] ?>"></td>
                <td><?= $item['name'] ?></td>
                <td class="price" data-val="<?= $item['price'] ?>"></td>
                <td>
                    <select class="qty ">
                        <?php for($i=1; $i<=20; $i++): ?>
                        <option value="<?=$i?>" <?= $i==$item['qty'] ? 'selected' : ''?>><?=$i?></option>
                        <?php endfor; ?>
                    </select>
                </td>
                <td class="sub-total"></td>
                <div><td button class="del"><div class="smail">刪除</div></button></td></div>
            </tr>
            <?php endforeach; ?>

            </tbody>
        </table>
            <div class="totle">總計: <strong id="amount"></strong></div>

            <?php if(isset($_SESSION['user'])): ?>
                <div class="totle ok">
                <a class="btn" href="member_pay.php">進入結帳</a></div>  
            <?php else: ?>

                <a class="btn" href="register.php">請先登入會員再結帳</a>
            <?php endif; ?>

        <?php else: ?>
            <div class="alert" role="alert">購物車裡沒有商品</div>
        <?php endif; ?>
    </div>

</div>
    <script>
        var dallorCommas = function(n){
            return '$ ' + n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        };
        $('.del').click(function(){
            var tr = $(this).closest('tr');
            var sid = tr.attr('data-sid');

            $.get('add_to_cart.php', {sid:sid}, function(data){
                // location.href = location.href; // reload page
                calItems(data);
                tr.remove();
                calTotalAmount();
            }, 'json');
        });

        $('.qty').on('change', function(){
            var tr = $(this).closest('tr');
            var sid = tr.attr('data-sid');
            var qty = $(this).val();
            // console.log( $(this).val(), tr.attr('data-sid') );

            $.get('add_to_cart.php', {sid:sid, qty:qty}, function(data){
                var sub = tr.find('.sub-total');
                var price = tr.find('.price').attr('data-val');
                sub.text( dallorCommas(qty * price) );
                calItems(data);
                calTotalAmount();
            }, 'json');
        });

        $('.sub-total').each(function(){
            var tr = $(this).closest('tr');
            var price_td = tr.find('.price');
            var price = price_td.attr('data-val');
            var qty = tr.find('.qty').val();
            price_td.text( dallorCommas( price ) );
            $(this).text( dallorCommas( price*qty ) );
        });

        // 計算總價
        var calTotalAmount = function(){
            var t = 0;
            $('.sub-total').each(function(){
                var tr = $(this).closest('tr');
                var price_td = tr.find('.price');
                var price = price_td.attr('data-val');
                var qty = tr.find('.qty').val();
                t += price*qty;
            });

            $('#amount').text( dallorCommas(t) );
        };
        calTotalAmount();
    </script>
<?php include __DIR__. '/__page_foot.php' ?>