<?php
session_start();

if(! isset($_SESSION['cart'])){
    $_SESSION['cart'] = array();
}

if(isset($_GET['sid'])){
    $sid = intval($_GET['sid']);
    // 應該要查詢 DB 看看有沒有這個商品


    if(empty( $_GET['qty'])) {
        unset($_SESSION['cart'][$sid]); //移除
    } else {
        $qty = intval($_GET['qty']);

        $_SESSION['cart'][$sid] = $qty; //加入商品 或者 修改數量
    }


}


echo json_encode($_SESSION['cart']);


// 查詢, 加入商品, 移除商品

// 查詢:沒有參數, 加入商品:sid和數量, 移除商品:sid

// print_r($_SESSION);