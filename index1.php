<?php
require __DIR__ . '/__connect_db.php';
$pname = 'index';
?>

<?php include __DIR__ . '/__page_head.php' ?>
<link type="text/css" rel="stylesheet" href="slick/slick/slick.css">
<link type="text/css" rel="stylesheet" href="slick/slick/slick-theme.css">
<link rel="stylesheet" type="text/css" href="css/indexwen.css">

<style>
    body {
        overflow: hidden; /*-------------auto*/
    }

    .header {
        opacity: 0; /*-------------*/
        transform: translateY(-105px); /*-105px*/
    }

    .navbar .logo {
        opacity: 0;
    }

    footer {
        opacity: 1;
    }

    footer {
        clear: right;
    }
</style>
<?php include __DIR__ . '/__page_header.php' ?>
<div class="container">

    <div class="ani_frame">
        <div class="perspective">
            <div class="ani_logo">
                <div class="mask fa"></div>
                <div class="mask nt"></div>
                <div class="mask a"></div>
                <div class="mask r"></div>
                <div class="o_pers">
                    <div class="o"></div>
                </div>
                <div class="mask i"></div>
                <div class="mask d"></div>
            </div>
            <div class="skip">
                <span>skip</span>
            </div>
        </div>
    </div><!--ani_frame-->

    <div class="main">
        <div class="content">
            <div class="slider">
                <img src="css/images/index/slider1.png" alt="">
                <img src="css/images/index/slider2.png" alt="">
                <img src="css/images/index/slider3.png" alt="">
            </div><!--slider-->
            <div class="index">

            </div>

            <div class="secone_new">商品情報</div>
            <div class="secone_new_slider slider2">
                <div class="slider_sec1">
                    <ul class="second_pic botton section1">
                        <a href="single-product.php?sid=11">
                            <li class="second">
                                <img class="sccond_imgshow" src="css/images/index/01.png">
                                <div class="second_sale">
                                    <p class="second_neme">名偵探柯南<span class="nt"> NT 1162</span></p>
                                    <p class="second_na">江戶川柯南</p>
                                </div>
                            </li>
                        </a>
                        <a href="single-product.php?sid=17">
                            <li class="second">
                                <img class="sccond_imgshow" src="css/images/index/1.png">
                                <div class="second_sale">
                                    <p class="second_neme">斬首循環<span class="nt"> NT 1339</span></p>
                                    <p class="second_na">玖渚友</p>
                                </div>
                            </li>
                        </a>
                        <a href="single-product.php?sid=18">
                            <li class="second">
                                <img class="sccond_imgshow" src="css/images/index/2.png">
                                <div class="second_sale">
                                    <p class="second_neme">排球少年!! <span class="nt">NT 1112</span></p>
                                    <p class="second_na">灰羽列夫</p>
                                </div>
                            </li>
                        </a>
                        <a href="single-product.php?sid=19">
                            <li class="second">
                                <img class="sccond_imgshow" src="css/images/index/3.png">
                                <div class="second_sale">
                                    <p class="second_neme">排球少年!! <span class="nt">NT 1061</span></p>
                                    <p class="second_na">夜久衛輔</p>
                                </div>
                            </li>
                        </a>
                    </ul>
                </div>
                <div class="slider_sec2">
                    <ul class="second_pic section2">
                        <a href="single-product.php?sid=20">
                            <li class="second ">
                                <img class="sccond_imgshow" src="css/images/index/4.png">
                                <div class="second_sale">
                                    <p class="second_neme">小愛<span class="nt">NT 1213</span></p>
                                    <p class="second_na">星期一的豐滿</p>
                                </div>
                            </li>
                        </a>
                        <a href="single-product.php?sid=21">
                            <li class="second">
                                <img class="sccond_imgshow" src="css/images/index/5.png">
                                <div class="second_sale">
                                    <p class="second_neme">櫻花任務 <span class="nt">NT 1213</span></p>
                                    <p class="second_na">木春由乃</p>
                                </div>
                            </li>
                        </a>
                        <a href="single-product.php?sid=22">
                            <li class="second">
                                <img class="sccond_imgshow" src="css/images/index/6.png">
                                <div class="second_sale">
                                    <p class="second_neme">排球少年!!<span class="nt">NT 1112</span></p>
                                    <p class="second_na">牛島若利</p>
                                </div>
                            </li>
                        </a>
                        <a href="single-product.php?sid=23">
                            <li class="second">
                                <img class="sccond_imgshow" src="css/images/index/7.png">
                                <div class="second_sale">
                                    <p class="second_neme">少女與戰車 <span class="nt">NT 1213</span></p>
                                    <p class="second_na">五十鈴華</p>
                                </div>
                            </li>
                        </a>
                    </ul>
                </div><!--------------sec2-->
            </div>

            <div class="secone_new">二手商品</div>
            <div class="second_hand_slider slider2">
                <div class="slider_sec3">
                    <ul class="second_pic botton section3">
                        <a href="single-product.php?sid=24">
                            <li class="second">
                                <img class="sccond_imgshow" src="css/images/index/8.png">
                                <div class="second_sale">
                                    <p class="second_neme">少女與戰車<span class="nt">NT 1213</span></p>
                                    <p class="second_na">秋山優花里</p>
                                </div>
                            </li>
                        </a>
                        <a href="single-product.php?sid=25">
                            <li class="second">
                                <img class="sccond_imgshow" src="css/images/index/9.png">
                                <div class="second_sale">
                                    <p class="second_neme">小魔女學園 <span class="nt">NT 1137</span></p>
                                    <p class="second_na">亞可·卡嘉莉</p>
                                </div>
                            </li>
                        </a>
                        <a href="single-product.php?sid=26">
                            <li class="second">
                                <img class="sccond_imgshow" src="css/images/index/10.png">
                                <div class="second_sale">
                                    <p class="second_neme">少女與戰車 <span class="nt">NT 1213</span></p>
                                    <p class="second_na"> 冷泉麻子</p>
                                </div>
                            </li>
                        </a>
                        <a href="single-product.php?sid=27">
                            <li class="second">
                                <img class="sccond_imgshow" src="css/images/index/11.png">
                                <div class="second_sale">
                                    <p class="second_neme">BML 2016 Ver. <span class="nt">NT 1137</span></p>
                                    <p class="second_na">33娘</p>
                                </div>
                            </li>
                        </a>
                    </ul>
                </div><!--sec1-->
                <div class="slider_sec4">
                    <ul class="second_pic last section4">
                        <a href="single-product.php?sid=28">
                            <li class="second">
                                <img class="sccond_imgshow" src="css/images/index/12.png">
                                <div class="second_sale">
                                    <p class="second_neme">LoveLive! Sunshine!!<span class="nt">NT 884</span></p>
                                    <p class="second_na"> 黑澤黛雅</p>
                                </div>
                            </li>
                        </a>
                        <a href="single-product.php?sid=29">
                            <li class="second">
                                <img class="sccond_imgshow" src="css/images/index/13.png">
                                <div class="second_sale">
                                    <p class="second_neme">LoveLive!<span class="nt">NT 1213</span></p>
                                    <p class="second_na">Lancer/斯卡哈</p>
                                </div>
                            </li>
                        </a>
                        <a href="single-product.php?sid=30">
                            <li class="second">
                                <img class="sccond_imgshow" src="css/images/index/14.png">
                                <div class="second_sale">
                                    <p class="second_neme">為美好的世界獻上祝福！2<span class="nt">NT 1137</span></p>
                                    <p class="second_na">惠惠</p>
                                </div>
                            </li>
                        </a>
                        <a href="single-product.php?sid=31">
                            <li class="second">
                                <img class="sccond_imgshow" src="css/images/index/15.png">
                                <div class="second_sale">
                                    <p class="second_neme">排球少年!!<span class="nt">NT 1162</span></p>
                                    <p class="second_na">赤葦京治</p>
                                </div>
                            </li>
                        </a>
                    </ul>
                </div><!--sec2-->
            </div><!--slider2-->

            <div class="secone_new">最新預購</div>
            <div class="pre_order_slider slider2">
                <div class="slider_sec5">
                    <ul class="second_pic botton section5">
                        <a href="single-product.php?sid=32">
                            <li class="second">
                                <img class="sccond_imgshow" src="css/images/index/16.png">
                                <div class="second_sale">
                                    <p class="second_neme">YURI!!!on ICE<span class="nt">NT 1137</span></p>
                                    <p class="second_na">維克多・尼基弗洛夫</p>
                                </div>
                            </li>
                        </a>
                        <a href="single-product.php?sid=33">
                            <li class="second">
                                <img class="sccond_imgshow" src="css/images/index/17.png">
                                <div class="second_sale">
                                    <p class="second_neme">BanG Dream! <span class="nt">NT 1137</span></p>
                                    <p class="second_na">戶山香澄</p>
                                </div>
                            </li>
                        </a>
                        <a href="single-product.php?sid=34">
                            <li class="second">
                                <img class="sccond_imgshow" src="css/images/index/18.png">
                                <div class="second_sale">
                                    <p class="second_neme">林克 荒野之息 <span class="nt">NT 1466</span></p>
                                    <p class="second_na"> 薩爾達傳說
                                    </p>
                                </div>
                            </li>
                        </a>
                        <a href="single-product.php?sid=35">
                            <li class="second">
                                <img class="sccond_imgshow" src="css/images/index/19.png">
                                <div class="second_sale">
                                    <p class="second_neme">刀劍亂舞-ONLINE <span class="nt">NT 884</span></p>
                                    <p class="second_na">
                                        三日月宗近 真劍必殺裝</p>
                                </div>
                            </li>
                        </a>
                    </ul>
                </div><!--slider_sec5-->
                <div class="slider_sec6">
                    <ul class="second_pic last section6">
                        <a href="single-product.php?sid=36">
                            <li class="second">
                                <img class="sccond_imgshow" src="css/images/index/20.png">
                                <div class="second_sale">
                                    <p class="second_neme">艦隊Collection
                                        <span class="nt">NT 1466</span></p>
                                    <p class="second_na"> Pola</p>
                                </div>
                            </li>
                        </a>
                        <a href="single-product.php?sid=37">
                            <li class="second">
                                <img class="sccond_imgshow" src="css/images/index/21.png">
                                <div class="second_sale">
                                    <p class="second_neme">艦隊Collection
                                        <span class="nt">NT 1794</span></p>
                                    <p class="second_na">厭戰號</p>
                                </div>
                            </li>
                        </a>
                        <a href="single-product.php?sid=38">
                            <li class="second">
                                <img class="sccond_imgshow" src="css/images/index/22.png">
                                <div class="second_sale">
                                    <p class="second_neme">艦隊Collection<span class="nt">NT 1263</span></p>
                                    <p class="second_na">吹雪改二</p>
                                </div>
                            </li>
                        </a>
                        <a href="single-product.php?sid=39">
                            <li class="second">
                                <img class="sccond_imgshow" src="css/images/index/23.png">
                                <div class="second_sale">
                                    <p class="second_neme">艦隊Collection<span class="nt">NT 1263</span></p>
                                    <p class="second_na">瑞鶴改</p>
                                </div>
                            </li>
                        </a>
                    </ul>
                </div><!--slider_sec6-->
            </div><!--pre_order_slider-->
            <button type="button" class="back-to-top">
                <span class="label">TOP</span>
            </button>
        </div> <!--content-->
    </div> <!--main-->
</div><!--container-->

<?php include __DIR__ . '/__page_foot.php' ?>


<script type="text/javascript" src="script/jquery-migrate-1.2.1.js"></script>
<script type="text/javascript" src="slick/slick/slick.js"></script>
<script>
    $(function () {

        /*
         * Slideshow
         */
        // 對每個有slideshow類別的元素進行處理
        $('.slider').each(function () {

            var $slides = $(this).find('img'), // 所有Slide
                slideCount = $slides.length,   // Slide個數
                currentIndex = 0;              // 目前Slide的index
            // 淡入顯示首張Slide
            $slides.eq(currentIndex).fadeIn();

            // 每7500毫秒就執行showNextSlide函數
            setInterval(showNextSlide, 2500);

            // 顯示下一張Slide的函數
            function showNextSlide() {

                //下張Slide的index
                //(如果是最後一張Slide，則會到第一張)
                var nextIndex = (currentIndex + 1) % slideCount;

                // 目前的Slide淡出顯示
                $slides.eq(currentIndex).fadeOut();

                // 下一張Slide淡入顯示
                $slides.eq(nextIndex).fadeIn();

                // 更新目前的index
                currentIndex = nextIndex;
            }

        });

    });

    //-------------------------------------------------TO TOP
    $scrollTop = $("body").scrollTop();
    $(window).scroll(function () {
        if ($(this).scrollTop() > 400) {
            $('.back-to-top').fadeIn("fast");
        } else {
            $('.back-to-top').stop().fadeOut("fast");
        }
    });

    /*
     * Back-toTop button (Smooth scroll)
     */
    $('.back-to-top').each(function () {


        // 檢測html、body何者為可捲動元素
        var $el = $(scrollableElement('html', 'body'));

        // 設定按鈕的點擊事件
        $(this).on('click', function (event) {
            event.preventDefault();
            $el.animate({scrollTop: 0}, 1000);
        });
    });

    // 透過scrollTop檢測可用元素的函數
    // http://www.learningjquery.com/2007/10/improved-animated-scrolling-script-for-same-page-links#update4
    function scrollableElement(elements) {
        var i, len, el, $el, scrollable;
        for (i = 0, len = arguments.length; i < len; i++) {
            el = arguments[i],
                $el = $(el);
            if ($el.scrollTop() > 0) {
                return el;
            } else {
                $el.scrollTop(1);
                scrollable = $el.scrollTop() > 0;
                $el.scrollTop(0);
                if (scrollable) {
                    return el;
                }
            }
        }
        return [];
    }


    //--------------------------------------------------------------------首頁開頭動畫
    /*-----------------------------------------變形中心點隨機*/
    var random_array = [];//--------------------儲存隨機6個數字陣列

    function random_origin() {
        $(".mask").each(function () {
            var random_1_100 = parseInt(Math.random() * 99 + 1); //每個mask隨機一次數字
            // console.log("random_1_100:" + random_1_100);
            var that = $(this);
            var ran_num = random_1_100;

//            console.log("ran_num:" + ran_num);
            if (0 < ran_num && ran_num <= 25) {
                /*-----------------------------------中心上-*/
                random_array.push(ran_num); //---------------把隨機數字丟到陣列
                that.css({
                    "transition": "all 0s",
                    "transform-origin": "top",
                    "transform": "scale(1,0)"
                });
//                that.html("<div>1~25,上</div><div>" + ran_num + "</div>");
            } else if (25 < ran_num && ran_num <= 50) {
                /*-----------------------------------中心下-*/
                random_array.push(ran_num);
                that.css({
                    "transition": "all 0s",
                    "transform-origin": "bottom",
                    "transform": "scale(1,0)"
                });
//                that.html("<div>26~50,下</div><div>" + ran_num + "</div>");

            } else if (50 < ran_num && ran_num <= 75) {
                /*-----------------------------------中心左-*/
                random_array.push(ran_num);
                that.css({
                    "transition": "all 0s",
                    "transform-origin": "left",
                    "transform": "scale(0,1)"
                });
//                that.html("<div>51~75,左</div><div>" + ran_num + "</div>");
            } else {
                /*-----------------------------------中心右-*/
                random_array.push(ran_num);
                that.css({
                    "transition": "all 0s",
                    "transform-origin": "right",
                    "transform": "scale(0,1)"
                });
//                that.html("<div>76~100,右</div><div>" + ran_num + "</div>");
            }
        });
// console.log("陣列:" + random_array);
    }

    function random_first_origin() {
        $(".mask").each(function () {
            var random_1_100 = parseInt(Math.random() * 99 + 1); //每個mask隨機一次數字
            // console.log("random_1_100:" + random_1_100);
            var that = $(this);
            var ran_num = random_1_100;

//            console.log("ran_num:" + ran_num);
            if (0 < ran_num && ran_num <= 25) {
                /*-----------------------------------中心上-*/
                random_array.push(ran_num); //---------------把隨機數字丟到陣列
                that.css({
                    "transition": "all 0s",
                    "transform-origin": "top",
                    "transform": "scale(1,1)"
                });
//                that.html("<div>1~25,上</div><div>" + ran_num + "</div>");
            } else if (25 < ran_num && ran_num <= 50) {
                /*-----------------------------------中心下-*/
                random_array.push(ran_num);
                that.css({
                    "transition": "all 0s",
                    "transform-origin": "bottom",
                    "transform": "scale(1,1)"
                });
//                that.html("<div>26~50,下</div><div>" + ran_num + "</div>");

            } else if (50 < ran_num && ran_num <= 75) {
                /*-----------------------------------中心左-*/
                random_array.push(ran_num);
                that.css({
                    "transition": "all 0s",
                    "transform-origin": "left",
                    "transform": "scale(1,1)"
                });
//                that.html("<div>51~75,左</div><div>" + ran_num + "</div>");
            } else {
                /*-----------------------------------中心右-*/
                random_array.push(ran_num);
                that.css({
                    "transition": "all 0s",
                    "transform-origin": "right",
                    "transform": "scale(1,1)"
                });
//                that.html("<div>76~100,右</div><div>" + ran_num + "</div>");
            }
        });
// console.log("陣列:" + random_array);
    }

    //---------------------------遮罩變短

    function mask_to_0() {
        $(".mask").each(function () {
            var get_num = random_array.shift();
            var that = $(this);

            if (0 < get_num && get_num <= 50) {
//                console.log("抓出陣列的數字" + get_num);
                that.css({
                    "transform": "scale(1,0)",
                    "transition": "all .25s"
                });
            } else {
//                console.log("抓出陣列的數字" + get_num);
                that.css({
                    "transform": "scale(0,1)",
                    "transition": "all .25s"
                });
            }
        });
    };
    //---------------------------遮罩變長
    function mask_to_100() {
        $(".mask").each(function () {
            var that = $(this);
            var get_num = random_array.shift();
//            console.log("抓出陣列的數字" + get_num);
            that.css({
                "transform": "scale(1,1)",
                "transition": "all .25s"
            });

        });
    }
    /*-----------------------------------------遮罩變形動畫*/
    var mask_speed_fast = 250;
    function mask_animate_fa() {
        setTimeout(random_first_origin, mask_speed_fast);
        setTimeout(mask_to_0, 300);
        setTimeout(random_origin, 600);
        setTimeout(mask_to_100, 750);//1000
    }


    /*-----------------------------------------執行次數*/
    var connect_time = 0;
    var mask_ani_time = 1000;
    function mask_ani_times(times) {
        for (i = 0; i < times; i++) {
            setTimeout(mask_animate_fa, mask_ani_time * i);
            connect_time += 1;
        }
    }

    /*--------------------------------------------------笑臉動畫*/
    /*------------往上動畫*/
    function o_ani_up() {

        var o_pers = $(".o_pers");
        o_pers.css({
            "transition": "all .4s cubic-bezier(.46,.08,.28,1.19) ",
            "transform": "translateY(0px) ",
            "opacity": "1"
        });
    };
    /*------------旋轉動畫*/
    function o_rotate() {
        var one_round = 360;
        var o_rotate = $(".o");
        o_rotate.css({
            "transition": "all .8s cubic-bezier(.46,.08,.28,1.19) ",
            "transform": "rotateY(" + one_round * 1.5 + "deg)"
        });
    }
    /*--------------------------------------------------整張LOGO動畫*/
    /*-------------LOGO淡入 + 變小 */
    var logo = $(".ani_logo");
    function LOGO_ani_far() {
        logo.css({
            "transition": "all .8s",
            "transform": "scale(.3,.3)",
            "opacity": "1"
            //console.log("connect_time:" + connect_time);
        });
    }

    /*-------------LOGO飛起來 + 旋轉 */
    function LOGO_ani_fly() {
        logo.css({
            "transition": "all .3s cubic-bezier(.55,.54,.6,.61)",
            "transform": "scale(.6,.6) rotateX(25deg) rotateY(15deg) rotateZ(0deg) translate3d(900px,-500px,0px)"
        });
    };


    function LOGO_ani_fly2() {
        logo.css({
            "transform-origin": "50% 50%",
            "transition": "all .23s cubic-bezier(0,.74,.2,1.01) ",
            "transform": "scale(.65,.65) rotateX(40deg) rotateY(11deg) rotateZ(4deg) translateX(500px) translateY(-800px) translateZ(0px)"
        });
    };

    function LOGO_ani_down() {
        var ani_frame = $(".ani_frame");
        logo.css({
            "transition": "all .2s cubic-bezier(.98,0,.17,1.02)",
            "transform": "scale(0.1201, 0.1201) rotateX(0deg) rotateY(0deg) rotateZ(0deg) translateX(-6767px) translateY(-4523px) translateZ(-1000px)"
        });
        ani_frame.css({
            "transition": "all .15s cubic-bezier(.98,0,.17,1.02)",
            "background": "rgba(143, 172, 202,0)"
        });

    };
    function LOGO_ani_start_fly() {
        LOGO_ani_far();
        LOGO_ani_fly();
        setTimeout(LOGO_ani_fly2, 350);
        setTimeout(LOGO_ani_down, 500);
    }

    //-----------------------------header + main 進場
    //----------------------------動畫LOGO消失
    function logo_out() {
        var ani_frame = $(".ani_frame");
        ani_frame.css({
            "transition": "all 0s",
            "display": "none"
        });
        ani_frame.remove();
    }
    //----------------------------header,main進場,body捲軸
    function body_in() {
        var header = $(".header");
        header.css({
            "transition": "all .5s",
            "transform": "translateY(0px)",
            "opacity": "1"
        });
        var main = $(".main");
        main.css({
            "transition": "all .5s",
            "opacity": "1"
        });
        var body = $("body");
        body.css({
            "overflow": "auto"
        });

        setTimeout(nav_bar_logo_in, 430);


        function nav_bar_logo_in() {
            var nav_bar_logo = $(".navbar .logo");
            nav_bar_logo.css({
                "opacity": "1"
            });
        }
    }
    function nav_bar_logo_in() {
        var nav_bar_logo = $(".navbar .logo");
        nav_bar_logo.css({
            "opacity": "1"
        });
    }
    //----------------------------header,body回位置
    function body_set() {
        var header = $(".header");
        var main = $(".main");

        header.css({
            "transform": " translateX(0px) translateY(0px) "
        });
        main.css({
            "transform": " translateX(0px) translateY(0px) "
        });
    }
    //----------------------------------------------加入TOP按鈕
    //                function append_top() {
    //                    $("body").append("<div class='to_top'>top</div>");
    //                }
    //                //    if(screen.width <767 ){
    //                append_top();
    //                //    }

    /*--------------------------------------------------串動畫*/

    var stid, stids = [];
    function open_ani() {
        LOGO_ani_far();
        mask_ani_times(2);
        stid = setTimeout(random_first_origin, mask_ani_time * connect_time + 200);
        stids.push(stid);
        stid = setTimeout(mask_to_0, mask_ani_time * connect_time + 300);
        stids.push(stid);
        stid = setTimeout(o_ani_up, mask_ani_time * connect_time);
        stids.push(stid);
        stid = setTimeout(o_rotate, mask_ani_time * connect_time + 100);
        stids.push(stid);
        stid = setTimeout(LOGO_ani_start_fly, 3300);
        stids.push(stid);
        stid = setTimeout(body_in, 3770);//4000
        stids.push(stid);
        stid = setTimeout(shake_times, 4235);
        stids.push(stid);
        stid = setTimeout(logo_out, 4280);
        stids.push(stid);
        stid = setTimeout(body_set, 5100);
        stids.push(stid);
//                    stid = setTimeout(append_top, 5200);
//                    stids.push(stid);

    }
    //-------------------------------------跳過動畫 !!!!!問老師!!!!!!!!!!!!!
    //解決: 動畫用一個變數存, 依次push到陣列

    $(".skip").click(function () {
        var ani_frame = $(".ani_frame");
        ani_frame.css("display", "none");
        body_in();
        nav_bar_logo_in();
        for (var i = 0; i < stids.length; i++) {
            clearTimeout(stids[i]);
        }
//                    append_top();
    });

    var window_width = $(window).width();
    //---------------------------------------------------------------動畫開關
    if (window_width > 767) {
        $(document).ready(function () {
            open_ani();
        });
    } else {
        $(".navbar .logo").css("transition", "all .25s")
        $(".ani_frame").css("display", "none")
        body_in();
    }
    //----------------------------------------------畫面震動
    var shack_px = 36;
    function shake_ani() {
        var random_1_4 = parseInt(Math.random() * 4 + 1);
//        console.log(random_1_4);
        var shack_time = 0.02;

        if (random_1_4 === 1) {
            $(".header").css({
                "transition": "all " + shack_time + "s ",
                "transform": "  translateX(" + shack_px + "px) translateY(" + shack_px + "px)"
            });
            $(".main").css({
                "transition": "all " + shack_time + "s ",
                "transform": "  translateX(" + shack_px + "px) translateY(" + shack_px + "px)"
            });
        } else if (random_1_4 === 2) {
            $(".header").css({
                "transition": "all " + shack_time + "s ",
                "transform": "  translateX(" + "-" + shack_px + "px) translateY(" + shack_px + "px)"
            });
            $(".main").css({
                "transition": "all " + shack_time + "s ",
                "transform": "  translateX(" + "-" + shack_px + "px) translateY(" + shack_px + "px)"
            });
        } else if (random_1_4 === 3) {
            $(".header").css({
                "transition": "all " + shack_time + "s  ",
                "transform": "  translateX(" + shack_px + "px) translateY(" + "-" + shack_px + "px)"
            });
            $(".main").css({
                "transition": "all " + shack_time + "s ",
                "transform": "  translateX(" + shack_px + "px) translateY(" + "-" + shack_px + "px)"
            });
        } else {
            $(".header").css({
                "transition": "all " + shack_time + "s  ",
                "transform": "  translateX(" + "-" + shack_px + "px) translateY(" + "-" + shack_px + "px)"
            })
            $(".main").css({
                "transition": "all " + shack_time + "s ",
                "transform": "  translateX(" + "-" + shack_px + "px) translateY(" + "-" + shack_px + "px)"
            });
        }
        shack_px--;
        if (shack_px === 0) {
            clearInterval(iid);
        }
    }
    //var iid = setInterval(shake_ani,20);
    function shake_times() {
        window.iid = setInterval(shake_ani, 20);
    }


    //------------------------------------------------首頁滾動動畫

        function index_ani() {
                //--------------------通用變數
                var window_scrollTop = $(window).scrollTop();
                // console.log("window_scrollTop :" + window_scrollTop);
                var window_height = $(window).height();
                var header_height = $("header").height();

                //--------------------------------第一層
                var section1 = $(".section1");

                var section1_off_top = section1.offset().top;
                // console.log("section1_off_top :" + section1_off_top);
                var section1_height = $(".section1").height();
                var section1_start = section1_off_top + section1_height * .6 - window_height - header_height;
                var sec1_a = $(".section1 a");
                var sec1_a2 = $(".section1 a:nth-child(2)");
                var sec1_a3 = $(".section1 a:nth-child(3)");
                var sec1_a4 = $(".section1 a:nth-child(4)");
                // console.log("section1_start :" + section1_start);
                if (window_scrollTop > section1_start) {
                    section1.css({
                        "transition": "all .8s ease",
                        "opacity": "1"
                    });
                    sec1_a.css({
                        "transition": "all .5s ease",
                        "transform": "translateY(0px)",
                        "opacity": "1"
                    });
                    sec1_a2.css("transition-delay", ".1s");
                    sec1_a3.css("transition-delay", ".2s");
                    sec1_a4.css("transition-delay", ".3s");
                }

                //-----------------------------第二層
                var section3 = $(".section3");
                var section3_off_top = section3.offset().top;
                var section3_height = $(".section3").height();
                var section3_start = section3_off_top + section3_height * .6 - window_height - header_height;
                var sec3_a = $(".section3 a");
                var sec3_a2 = $(".section3 a:nth-child(2)");
                var sec3_a3 = $(".section3 a:nth-child(3)");
                var sec3_a4 = $(".section3 a:nth-child(4)");
                if (window_scrollTop > section3_start) {
                    section3.css({
                        "transition": "all .8s ease",
                        "opacity": "1"
                    });
                    sec3_a.css({
                        "transition": "all .5s ease",
                        "transform": "translateY(0px)",
                        "opacity": "1"
                    });
                    sec3_a2.css("transition-delay", ".1s");
                    sec3_a3.css("transition-delay", ".2s");
                    sec3_a4.css("transition-delay", ".3s");
                }
                //------------------------------------------第三層
                var section5 = $(".section5");
                var section5_off_top = section5.offset().top;
                var section5_height = $(".section5").height();
                var section5_start = section5_off_top + section5_height * .6 - window_height - header_height;
                var sec5_a = $(".section5 a");
                var sec5_a2 = $(".section5 a:nth-child(2)");
                var sec5_a3 = $(".section5 a:nth-child(3)");
                var sec5_a4 = $(".section5 a:nth-child(4)");
                if (window_scrollTop > section5_start) {
                    section5.css({
                        "transition": "all .8s ease",
                        "opacity": "1"
                    });
                    sec5_a.css({
                        "transition": "all .5s ease",
                        "transform": "translateY(0px)",
                        "opacity": "1"
                    });
                    sec5_a2.css("transition-delay", ".1s");
                    sec5_a3.css("transition-delay", ".2s");
                    sec5_a4.css("transition-delay", ".3s");
                }
        }
    if (screen.width > 767){
        $(window).scroll(function () {
            index_ani() ;
        });
    }else{
        var section1 = $(".section1");
        var sec1_a = $(".section1 a");
        var sec1_a2 = $(".section1 a:nth-child(2)");
        var sec1_a3 = $(".section1 a:nth-child(3)");
        var sec1_a4 = $(".section1 a:nth-child(4)");
        var section3 = $(".section3");
        var sec3_a = $(".section3 a");
        var sec3_a2 = $(".section3 a:nth-child(2)");
        var sec3_a3 = $(".section3 a:nth-child(3)");
        var sec3_a4 = $(".section3 a:nth-child(4)");
        var section5 = $(".section5");
        var sec5_a = $(".section5 a");
        var sec5_a2 = $(".section5 a:nth-child(2)");
        var sec5_a3 = $(".section5 a:nth-child(3)");
        var sec5_a4 = $(".section5 a:nth-child(4)");
        $(document).ready(function(){
            section1.css({
                "transition": "all .8s ease",
                "opacity": "1"
            });
            sec1_a.css({
                "transition": "all .5s ease",
                "transform": "translateY(0px)",
                "opacity": "1"
            });
            sec1_a2.css("transition-delay", ".1s");
            sec1_a3.css("transition-delay", ".2s");
            sec1_a4.css("transition-delay", ".3s");
            section3.css({
                "transition": "all .8s ease",
                "opacity": "1"
            });
            sec3_a.css({
                "transition": "all .5s ease",
                "transform": "translateY(0px)",
                "opacity": "1"
            });
            sec3_a2.css("transition-delay", ".1s");
            sec3_a3.css("transition-delay", ".2s");
            sec3_a4.css("transition-delay", ".3s");
            section5.css({
                "transition": "all .8s ease",
                "opacity": "1"
            });
            sec5_a.css({
                "transition": "all .5s ease",
                "transform": "translateY(0px)",
                "opacity": "1"
            });
            sec5_a2.css("transition-delay", ".1s");
            sec5_a3.css("transition-delay", ".2s");
            sec5_a4.css("transition-delay", ".3s");
        });
    }
    //------------------------------------------slicker
    if (screen.width > 767) {
        $('.slider2').slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            draggable: false,
            speed: 780,
            fade: false,
            adaptiveHeight: false,
            autoplay: false,
            autoplaySpeed: 4500,
            cssEase: "ease-out",
            prevArrow: '<img src="css/images/index/right.png"  data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button">',
            nextArrow: '<img src="css/images/index/right.png" class="slick-next"></button>',
            focusOnSelect: false,
            easing: "ease-in"
        });
    }
    //    else {
    //        $('.slider2').slick({
    //            infinite: true,
    //            slidesToShow: 1,
    //            slidesToScroll: 1,
    //            dots: true,
    //            speed: 780,
    //            fade: false,
    //            adaptiveHeight: false,
    //            autoplay: false,
    //            autoplaySpeed: 4500,
    //            cssEase: "ease-out",
    //            prevArrow: '<img src="css/images/index/right.png"  data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button">',
    //            nextArrow: '<img src="css/images/index/right.png" class="slick-next"></button>',
    //            focusOnSelect: false,
    //            easing: "ease-in"
    //        });
    //    }


    //    /*-----------------------------------------------------slider*/
    //$('.slider').slick({
    //    infinite: true,
    //    slidesToShow: 1,
    //    slidesToScroll: 1,
    //    dots: true,
    //    speed: 780,
    //    fade: false,
    //    adaptiveHeight: false,
    //    autoplay: false,
    //    autoplaySpeed: 4500,
    //    cssEase: "ease-out",
    //    prevArrow: '<img src="css/images/index/left.png"  data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button">',
    //    nextArrow: '<img src="css/images/index/left.png" class="slick-next"></button>',
    //    focusOnSelect: false,
    //    easing: "ease-in"
    //});


    //--------------------------------------------------to_top
    //                $(window).scroll(function () {
    //                    var window_scroll_top = $(window).scrollTop();
    //                    var document_height = $(document).height();
    //                    var top_show = document_height / 2;
    //                    var top_show2 = top_show - screen.height / 2;
    ////        console.log("top_show"+ top_show);
    //                    var to_top = $('.to_top');
    //                    if (screen.width > 767) {
    //                        if (window_scroll_top >= top_show2) {
    //                            to_top.css({
    //                                "transition": "all .35s  ",
    //                                "opacity": " 1 ",
    //                                "transform": " translateX(0px)"
    //                            })
    //                        } else {
    //                            to_top.css({
    //                                "transition": "all .35s  ",
    //                                "opacity": " 0 ",
    //                                "transform": " translateX(120px)"
    //                            })
    //                        }
    //                    } else {
    //                        if (window_scroll_top >= top_show2) {
    //                            to_top.css({
    //                                "transition": "all .35s  ",
    //                                "opacity": " 1 ",
    //                                "transform": " translateX(0px)"
    //                            })
    //
    //                        } else {
    //                            to_top.css({
    //                                "transition": "all .35s  ",
    //                                "opacity": " 0 ",
    //                                "transform": " translateX(-120px)"
    //                            })
    //                        }
    //                    }
    //                });

    //-------------------按下回上
    //                $(".to_top").click(function () {
    //                    console.log("to_top");
    //                    $('header').ScrollTo({
    //                        duration: 700,
    //                        easing: 'linear'
    //                    });
    //                });
    //                $(".arrow_dowm img").click(function () {
    //                    $('#img_top_empty').ScrollTo({
    //                        duration: 1000,
    //                        easing: 'linear'
    //                    });
    //                });
</script>
