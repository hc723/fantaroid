<?php
require __DIR__ . '/__connect_db.php';
$pname = 'single-product';


$sid = isset($_GET['sid']) ? (int)$_GET['sid'] : 0;

$sql = "SELECT * FROM `products` WHERE `sid`=$sid";
$rs = $mysqli->query($sql);
$row = $rs->fetch_assoc();

?>

<head>
<link type="text/css" rel="stylesheet" href="slick/slick/slick.css">
<link type="text/css" rel="stylesheet" href="slick/slick/slick-theme.css">
<link rel="stylesheet" type="text/css" href="css/product_detaila.css">
 <?php include __DIR__. "/__page_head.php" ?>
</head>
<style>
	.slick-list.draggable{
		max-height: 253px;
	}
	.qty{
		height: 30px;
		width: 60px;
		font-size: 1em;
	}
</style>

<body>
<?php include __DIR__. '/__page_header.php' ?>	
<div class="main">
        <div class="content">
        <?php if(empty($row)): ?>
            <div class="alert" role="alert">沒有商品資料</div>
        <?php else: ?>
            <div class="item_main">
                <span class="item_left">
                    <h2><?= $row['name'] ?></h2>
                    <h5><?= $row['title'] ?></h5>
                    <p class="por_txt"><?= $row['por_txt'] ?></p>
                    <p>規格:<span class="pro_name"><?= $row['pro_name'] ?></span></p>
                    <p>尺寸:<span class="works_name"><?= $row['works_name'] ?></span></p>
                    <p>附件:<span class="manufacturer"><?= $row['manufacturer'] ?></span></p> 
                    <p>原型製作:<span class="specification"><?= $row['specification'] ?></span></p>
                    <p>價格:<span class="price"><?= $row['price'] ?></span></p>
                </span>
                <span class="item_right">
                    <div class="picture">
                        <div class="picture_l active">
                            <span class="pic_l_frame">
                                <img src="<?= $row['img_id'] ?>" alt="">
                            </span>
                        </div>
                        <section class="picture_s">
                            <div>
                                <img class="s_img_l" src="css/images/product/in/<?= $row['sid'] ?>-1.jpg" alt="">
                            </div>
                            <div>
                                <img class="s_img_m" src="css/images/product/in/<?= $row['sid'] ?>-2.jpg" alt="">
                            </div>
                            <div>
                                <img class="s_img_r" src="css/images/product/in/<?= $row['sid'] ?>-3.jpg" alt="">
                            </div>
                        </section>
                        
                        <div class="button">
	                        <select name="qty" class="qty">
	                            <option value="1">1</option>
	                            <option value="2">2</option>
	                            <option value="3">3</option>
	                            <option value="4">4</option>
	                            <option value="5">5</option>
	                            <option value="6">6</option>
	                            <option value="7">7</option>
	                            <option value="8">8</option>
	                            <option value="9">9</option>
	                        </select>
	                        <span class="buy_btn" data-sid="<?= $row['sid'] ?>">加入購物車</span>
	                        <span class="favor">加入願望清單</span>
                        </div>
                    </div>
                </span>
            </div>
        <?php endif; ?>

		<div class="similar">
			<div class="similar_header">
				相似商品
			</div>
			<div class="similar_header_bar"></div>
			<div class="similar_product">
				<div class="similar_products">
					<div>
						<img src="css/images/product/01.jpg" alt="">
					</div>
					<div>
						<img src="css/images/product/02.jpg" alt="">
					</div>
					<div>
						<img src="css/images/product/03.jpg" alt="">
					</div>
					<div>
						<img src="css/images/product/04.jpg" alt="">
					</div>
					<div>
						<img src="css/images/product/01.jpg" alt="">
					</div>
					<div>
						<img src="css/images/product/01.jpg" alt="">
					</div>
					<div>
						<img src="css/images/product/01.jpg" alt="">
					</div>
					<div>
						<img src="css/images/product/01.jpg" alt="">
					</div>
				</div>
			</div>
		</div> <!---similar -->
	</div><!-- content -->
</div><!-- main -->
<?php include __DIR__. "/__page_foot.php" ?> 


<script type="text/javascript" src="script/jquery-migrate-1.2.1.js"></script>
<script type="text/javascript" src="slick/slick/slick.js"></script>

<script type="text/javascript">
//------------------------------------------------------------------左區塊長-//
	var window_width = $(window).width();
	// if( $(window).width() > 767 ){
	// 	var window_width = $(window).width();
	// 	$(document).ready(function(){
			
	// 		var item_right_h = $(".item_right").height();
	// 		var item_left = $(".item_left");
	// 		var ww = window_width;
	// 		item_left.css("height", item_right_h + "px")
	// 		$(window).mousemove(function(){
	// 			var window_width = $(window).width();
	// 			var item_right_h = $(".item_right").height();
	// 			var item_left = $(".item_left");
	// 			var ww = window_width;
	// 			item_left.css("height", item_right_h + "px")
	// 		});
	// 	});
	// };
 </script>

 <script type="text/javascript">
  //-----------------------------------------------------------slick外掛
    $(document).ready(function(){
    	if(window_width <=767){
      $('.similar_products').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: false,
		speed: 500,
		fade: false,
		cssEase:"ease-out",
		prevArrow:'<img src="css/images/product/left.png"  data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button">',
		nextArrow:'<img src="css/images/product/right.png" class="slick-next"></button>',
		focusOnSelect:false,
		easing:"ease-out",
		});
       }else{
       	$('.similar_products').slick({
		infinite: true,
		slidesToShow: 4,
		slidesToScroll: 4,
		dots: true,
		speed: 500,
		fade: false,
		cssEase:"ease-out",
		prevArrow:'<img src="css/images/product/left.png"  data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button">',
		nextArrow:'<img src="css/images/product/right.png" class="slick-next"></button>',
		focusOnSelect:false,
		});

       }
  	});
 </script>
 <script>
 //----------------------------------------------------------------圖片置換


	
		var s_img = $(".picture_s div img");
		var L_img = $(".picture_l span img");
		s_img.on("click", function(){
			var that = $(this);
			if( $(window).width() > 767 ){
		   		var L_img_src = $('.picture_l img').attr("src")
				var thatsrc = $(this).attr("src");
				that.fadeOut(200,"swing",
					function(){
						that.fadeIn(100,"swing");
                        that.attr("src",L_img_src);
				    });
						L_img.animate({opacity: 0},200,"swing",
					function(){
						L_img.animate({opacity:1},200,"swing");
						L_img.attr("src",thatsrc);
				});
			}else{
					$(".picture_l").removeClass("active");
					s_img.removeClass("active");
					that.addClass("active");
			};
		});

		if( $(window).width() < 767 ){
			$(".picture_l").click(function(){
				if( s_img.hasClass("active") ){
					s_img.removeClass("active");
					$(".picture_l").addClass("active");
				};
			});
		};
//-----------------購物車------------------------//
$('.buy_btn').click(function(){
    var sid = $(this).attr('data-sid');
    var qty = $(this).closest('.button').find('.qty').val();
    var name = $('h2').text();
    console.log(name);
    $.get('add_to_cart.php', {sid:sid, qty:qty}, function(data){
        alert(name + ' 已加入購物車');
        calItems(data); // 計算並顯示總數量
    }, 'json');


});

 </script>

</body>
</html>