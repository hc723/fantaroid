<!DOCTYPE html>
<html lang="zh">
<head>
<link type="text/css" rel="stylesheet" href="slick-1.6.0/slick/slick.css">
<link type="text/css" rel="stylesheet" href="slick-1.6.0/slick/slick-theme.css">
<link rel="stylesheet" type="text/css" href="css/product_detaila.css">
 <?php include __DIR__. "/__page_head.php" ?>
</head>

<body>
<?php include __DIR__. "/__page_header.php" ?>	
<div class="main">
	<div class="content">
		<div class="item_main">
			<span class="item_left">
				<h2>魔法少女</h2>
				<h5>美樹沙耶香</h5>
				<p class="por_txt">
				「美樹沙耶香」以舞妓之姿黏土人化！
<br>『魔法少女小圓』中的「美樹沙耶香」以涼爽風格的舞妓之姿變身成黏土人登場。表情零件有「笑臉」與「閉眼臉」。另附「開/闔」兩種和傘。搭配替換用「坐姿零件」與「閉眼臉」還能擺出跪坐敬禮的動作。與另售的『魔法少女小圓』舞妓系列黏土人一同擺飾，賞玩各式各樣的情境吧！			</p>

				<p>規格:
					<span class="pro_name">ABS、PVC 塗裝完成可動模型</span>
				</p>
				<p>尺寸:
					<span class="works_name">高約百毫米</span>
				</p>
				<p>附件:
				<span class="manufacturer">附專用台座</span>
				</p> 
				<p>原型製作
					<span class="specification">揉(Hachioto)</span>
				</p>
				<p>價格:
					<span class="price">NT1213</span>
				</p>
			</span>
			<span class="item_right">
				<div class="picture">
					<div class="picture_l active">
						<span class="pic_l_frame">
							<img src="css/images/productindex02/4.png" alt="美樹沙耶香">
						</span>
					</div>
					<section class="picture_s">
						<div>
							<img class="s_img_l" src="css/images/productindex02/1.png" alt="美樹沙耶香">
						</div>
						<div>
							<img class="s_img_m" src="css/images/productindex02/2.png" alt="美樹沙耶香">
						</div>
						<div>
							<img class="s_img_r" src="css/images/productindex02/3.png" alt="美樹沙耶香">
						</div>
					</section>
					<div class="button">
					<span>加入購物車</span>
					<span class="favor">加入願望清單</span>
				</div>
				</div>
				
			</span>
		</div>

		<div class="similar">
			<div class="similar_header">
				相似商品
			</div>
			<div class="similar_header_bar"></div>
			<div class="similar_product">
				<div class="similar_products">
					<div>
						<img src="css/images/productindex02/5.jpg" alt="">
					</div>
					<div>
						<img src="css/images/productindex02/6.jpg" alt="">
					</div>
					<div>
						<img src="css/images/productindex02/7.jpg" alt="">
					</div>
					<div>
						<img src="css/images/productindex02/8.jpg" alt="">
					</div>
					<div>
						<img src="css/images/productindex02/9.jpg" alt="">
					</div>
					
					<div>
						<img src="css/images/productindex02/10.jpg" alt="">
					</div>
					<div>
						<img src="css/images/productindex02/11.jpg" alt="">
					</div>
					<div>
						<img src="css/images/productindex02/12.jpg" alt="">
					</div>
					<div>
						<img src="css/images/productindex02/13.jpg" alt="">
					</div>
					<div>
						<img src="css/images/productindex02/14.jpg" alt="">
					</div>
					<div>
						<img src="css/images/productindex02/10.jpg" alt="">
					</div>
					<div>
						<img src="css/images/productindex02/11.jpg" alt="">
					</div>
				</div>
			</div>
		</div> <!---similar -->
	</div><!-- content -->
</div><!-- main -->
<?php include __DIR__. "/__page_footer.php" ?> 


<script type="text/javascript" src="script/jquery-migrate-1.2.1.js"></script>
<script type="text/javascript" src="slick-1.6.0/slick/slick.js"></script>

<script type="text/javascript">
//------------------------------------------------------------------左區塊長-//
	var window_width = $(window).width();
	// if( $(window).width() > 767 ){
	// 	var window_width = $(window).width();
	// 	$(document).ready(function(){
			
	// 		var item_right_h = $(".item_right").height();
	// 		var item_left = $(".item_left");
	// 		var ww = window_width;
	// 		item_left.css("height", item_right_h + "px")
	// 		$(window).mousemove(function(){
	// 			var window_width = $(window).width();
	// 			var item_right_h = $(".item_right").height();
	// 			var item_left = $(".item_left");
	// 			var ww = window_width;
	// 			item_left.css("height", item_right_h + "px")
	// 		});
	// 	});
	// };
 </script>

 <script type="text/javascript">
  //-----------------------------------------------------------slick外掛
    $(document).ready(function(){
    	if(window_width <=767){
      $('.similar_products').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: false,
		speed: 500,
		fade: false,
		cssEase:"ease-out",
		prevArrow:'<img src="css/images/product/left.png"  data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button">',
		nextArrow:'<img src="css/images/product/right.png" class="slick-next"></button>',
		focusOnSelect:false,
		easing:"ease-out",
		});
       }else{
       	$('.similar_products').slick({
		infinite: true,
		slidesToShow: 4,
		slidesToScroll: 4,
		dots: true,
		speed: 500,
		fade: false,
		cssEase:"ease-out",
		prevArrow:'<img src="css/images/product/left.png"  data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button">',
		nextArrow:'<img src="css/images/product/right.png" class="slick-next"></button>',
		focusOnSelect:false,
		});

       }
  	});
 </script>
 <script>
 //----------------------------------------------------------------圖片置換


	
		var s_img = $(".picture_s div img");
		var L_img = $(".picture_l span img");
		s_img.on("click", function(){
			var that = $(this);
			if( $(window).width() > 767 ){
		   		var L_img_src = $('.picture_l img').attr("src")
				var thatsrc = $(this).attr("src");
				that.fadeOut(200,"swing",
					function(){
						that.fadeIn(100,"swing");
                        that.attr("src",L_img_src);
				    });
						L_img.animate({opacity: 0},200,"swing",
					function(){
						L_img.animate({opacity:1},200,"swing");
						L_img.attr("src",thatsrc);
				});
			}else{
					$(".picture_l").removeClass("active");
					s_img.removeClass("active");
					that.addClass("active");
			};
		});

		if( $(window).width() < 767 ){
			$(".picture_l").click(function(){
				if( s_img.hasClass("active") ){
					s_img.removeClass("active");
					$(".picture_l").addClass("active");
				};
			});
		};

 </script>

</body>
</html>