<footer class="footer">
<div class="footer-basic-centered">

        <div class="footer_menu">
            <div>
                <li><a href="aboutus.php"><p class="footer-company-motto">關於我們</p></a></li>
                <li><p class="footer-company-motto">客戶服務</p></li>
                <?php if(isset($_SESSION['user'])): ?>
                    <li><a href="member_login.php"><p class="footer-company-motto">會員專區</p></a></li>
                <?php else: ?>
                    <li><a href="register.php"><p class="footer-company-motto">會員專區</p></a></li>
                <?php endif; ?>
            </div>
        </div>
    <div class="flex_iconadd">
        <div class="add_copy">
            <!-- <p class="footer-company-name">
                <i class="fa fa-home" aria-hidden="true">
                    <span>周一至周日 10:00-19:00 &nbsp</span>
                </i>
                <i class="fa fa-map-marker" aria-hidden="true">
                    <span>台北市復興南路一段390號15樓 &nbsp</span>
                </i>
                <i class="fa fa-phone" aria-hidden="true">
                    <span>02-2708-9215 &nbsp </span>
                </i>
                <i class="fa fa-envelope" aria-hidden="true">
                    <span>master@iii.org.tw </span>
                </i>
            </p> -->
            <p class="footer-links">
            <img src="css/images/iconshare.png" alt="分享連結"></p>
            
        </div>
        <p class="footer-company-name">Copyright &copy; 2017 fantaroid Inc. All rights reserved</p>
         
    </div>
</div>
</footer>
<script type="text/javascript" src="script/jquery-3.2.1.js" charset="UTF-8"></script>

</body>
</html>