<!doctype html>
<html lang="UTF-8">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet" href="css/reset.css">
<link type="text/css" rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="css/footer.css">
<link rel="stylesheet" type="text/css" href="css/header.css">
<script type="text/javascript" src="script/jquery-3.2.1.js" charset="UTF-8"></script>
<title>Login, Sign up Header</title>

<style>
    body{
      font-family:Arial, Helvetica, "微軟正黑體","新細明體","細明體",sans-serif;
      font-size:24px;
      margin:0 ;
      padding:0 ;
      position: relative;
      max-width: 100vw;
  }
    ul, li {
      margin:0 ;
      padding:0 ;
      position: relative;
  }
  @media screen and (max-width:767px) {
    body{
      padding:0 ;
    }
  }
</style>
</head>

