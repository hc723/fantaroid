<?php
require __DIR__ . '/__connect_db.php';
$pname = 'register';

//註冊
if( isset($_POST['email']) ){

    $hash = sha1($_POST['email']. uniqid());
    $sql = "INSERT INTO `members`(
         `name`,
         `email`, 
         `password`, 
         `mobile`, 
         `hash`, 
         `created_at`
          ) VALUES ( ?, ?, ?, ?, '$hash', NOW() )";
   
    $stmt = $mysqli->prepare($sql);
    if($mysqli->error){
        echo $mysqli->error;
        exit;
    }
    $stmt->bind_param("ssss",
        $_POST['name'] ,
        $_POST['email'] ,
        $_POST['password'],
        $_POST['mobile']
    );

    $stmt->execute();

    $result = $stmt->affected_rows;

}

//登入
if( isset($_POST['email2']) ){
    $sql = sprintf("SELECT * FROM `members` WHERE `email`='%s' AND `password`='%s'",
        $mysqli->escape_string($_POST['email2']),
        $_POST['password2']
        );

    //echo $sql;

    $rs = $mysqli->query($sql);
    $row = $rs->fetch_assoc();
    if($row){
        $_SESSION['user'] = $row;
        $msg = '登入成功';
        header("Location: ./");

        if(isset($_SESSION['come_from'])){
            header("Location: ". $_SESSION['come_from']);
            unset($_SESSION['come_from']);
        } else {
            header("Location: ./");
        }
        exit;
    } else {
        $msg = '密碼錯誤';
    }
} else {
    $_SESSION['come_from'] = $_SERVER['HTTP_REFERER'];
}


?>
<?php include __DIR__ . '/__page_head.php' ?>

    <style>
        .red {
            color: red;
            display: none;
        }
        /*跳出訊息視窗的設定*/
        .alert{
            position: absolute;
            z-index: 1;
            margin-left: -100px;
            width: 200px;
            height: 50px;
            text-align: center;
            line-height: 50px;
            font-size: 20px;
            color: #fff;
            background-color: rgba(251,129,52,.8); 
        }
    </style>
    <div class="container">
        <?php include __DIR__ . '/__page_header.php' ?>
        <link rel="stylesheet" type="text/css" href="css/member.css">

        <div class="member_all">
            <p class="member_only">會員專區</p>
        </div>

        <div class="login_open">
            <div class="login">
                <p class="member_a">會員註冊</p>

                <form method="post" onsubmit="return checkForm();">
                    <div>
                        <input id="key" name="name" type="text" placeholder="姓名" value="<?= empty($_POST['name']) ? '' : htmlentities($_POST['name']) ?>" size="22" /><br>
                        <input id="key" name="mobile" type="text" placeholder="手機" value="<?= empty($_POST['mobile']) ? '' : htmlentities($_POST['mobile']) ?>" size="22" /><br>
                        <input id="key" name="email" type="text" placeholder="Email" value="<?= empty($_POST['email']) ? '' : htmlentities($_POST['email']) ?>" size="22" /><br>
                        <input id="key" name="password" type="password" placeholder="密碼" value="<?= empty($_POST['password']) ? '' : htmlentities($_POST['password']) ?>" size="22" /><br>
                        <input id="key" type="password" placeholder="確認密碼" size="22" /><br>
                        <div class="check_box">
                            <a href=""><button type="submit" id="check" class="">註冊</button></a>
                        </div>
                    </div>
                </form>
            </div>
            <?php if(isset($result)): ?>
                <?php if($result==1): ?>
                    <div class="col-md-12" id="myinfo">
                        <div class="alert" role="alert">
                            資料新增完成
                        </div>
                    </div>

                    <?php elseif($result==-1): ?>
                    <div class="col-md-12" id="myinfo">
                        <div class="alert" role="alert">
                            email 已經被使用過了
                        </div>
                    </div>
                <?php endif; ?>
                <script>
                    setTimeout(function(){
                        $('#myinfo').slideUp();
                    }, 3000);
                </script>
            <?php endif; ?>

            <?php if(isset($msg)): ?>
                <div class="col-md-12" id="myinfo">
                    <div class="alert" role="alert">
                        <?= $msg ?>
                    </div>
                </div>
                <script>
                    setTimeout(function(){
                        $('#myinfo').slideUp();
                    }, 3000);
                </script>
            <?php endif; ?>

            <div class="input"> 
                <p class="member_a">會員登入</p>
                <form method="post" onsubmit="return checkForm();">
                    <div>
                        <input id="key" name="email2" type="text" placeholder="帳號email" value="<?= empty($_POST['email2']) ? '' : htmlentities($_POST['email2']) ?>" size="22" /><br>
                        <input id="key" name="password2" type="password" placeholder="密碼"  value="<?= empty($_POST['password2']) ? '' : htmlentities($_POST['password2']) ?>" size="22" /><br>
                        <a href=""><div class="password">忘記密碼？</div></a>
                        <div class="check_box">
                            <a href="member_login.php"><button type="submit" id="check" class="">登入</button></a>
                        </div>
                    </div>
                </form>
            </div>
        </div>


    <script>
 

    </script>
<?php include __DIR__ . '/__page_foot.php' ?>