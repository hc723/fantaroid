<?php
require __DIR__ . '/__connect_db.php';
$pname = 'member_login';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta hprop-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="css/reset.css">
	<link rel="stylesheet" type="text/css" href="css/member_login.css">
<?php include __DIR__. "/__page_head.php" ?>
<title>我的頁面-登入</title>
</head>
<body>
  <?php include __DIR__. '/__page_header.php' ?>
<div class="member_login_all">
<?php if(isset($_SESSION['user'])): ?>
  <h1 class="member_login_cart"><?= $_SESSION['user']['name'] ?></h1>
<?php endif; ?>
  <div class="container">
    <a href="#" class="showall">全部展開</a> | <a href="#" class="hideall">全部隱藏</a>
    <dl>
      <dt class="member_title"><a class="font_c" href="#">我的訂單</a></dt>
      <dd>
        <ul>
          <li>
            <a class="dis" href="">
            	<p>訂單編號</p>
            	<p>訂購時間</p>
            	<p>訂購金額</p>
            	<p>商品名稱</p>
            	<p>處理狀態</p>
            </a>
          </li>
        </ul>
      </dd>
      <dt class="member_title"><a  class="font_c" href="#">我的最愛</a></dt>
      <dd>
        <ul>
          <li>
            <a class="dis" href="">
            	<p>商品名稱</p>
            	<p>發行日期</p>
            	<p>商品價格</p>
            	<p>變更</p>
            </a>
          </li>
        </ul>
      </dd>
      <dt class="member_title"><a  class="font_c" href="#">購物金</a></dt>
      <dd>
        <ul>
          <li>
            <a class="dis" href="">
          	 <p>可用金額</p>
            </a>
          </li>
        </ul>
      </dd>
      <dt class="member_title"><a class="font_c" href="#">會員資料修改</a></dt>
      <dd>
        <ul>
          <li>
            <a class="member_info" href="">
            	<p>姓名</p>
            	<p>手機</p>
            	<p>密碼</p>
            </a>
          </li>
        </ul>
      </dd>
    </dl>
  	<div class="process_buttom">
			<a class="process_back" href="procuctindex.php">回首頁</a>
			<a class="process_willsale" href="logout.php">登出</a>
  	</div>
  </div>
</div>
<?php include __DIR__. "/__page_foot.php" ?>
<script>
	$("dd").hide();

//按下dt元素連結
$("dt a").click(function() {
    $("dd").slideUp("slow"); //子分頁
    
    //下一個主選單以下的內容要slide down
    $(this).parent().next().slideDown("slow");
    return false;
});

// 全部展開
    $('.showall').click(function(){
      $('dd').slideDown();
      return false;
    });

    // 全部隱藏
    $('.hideall').click(function(){
      $('dd').slideUp();
      return false;
    });


</script>
</body>
</html>