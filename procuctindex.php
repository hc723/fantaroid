<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="css/reset.css">
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>
	<link rel="stylesheet" type="text/css" href="slick/slick-theme.css"/>
	<link rel="stylesheet" type="text/css" href="css/productindex.css">
	<link rel="stylesheet" type="text/css" href="slick/slick.css"/>
 <?php include __DIR__. "/__page_head.php" ?>
	<title>second</title>
<?php include __DIR__. "/__page_header.php" ?>
<body>
	
	<div class="secone_new"><span class="font_one">8</span>月</div>
		<ul class="second_pic botton">
			<a href="__page_product_detail.php"><li class="second">
				<img class="sccond_imgshow" src="css/images/productindex/1.png">
				<div class="second_sale">
					<p class="second_neme">魔法少女<span class="nt"> NT 1203</span></p>
					<p class="second_na">美樹沙耶香 舞妓Ver</p>
				</div>
			</li></a>		
			<li class="second">
				<img class="sccond_imgshow" src="css/images/productindex/1-2.png">
				<div class="second_sale">
					<p class="second_neme">你的名字  <span class="nt">NT 885</span></p>
					<p class="second_na">立花瀧</p>
				</div>	
			</li>
			<li class="second">
				<img class="sccond_imgshow" src="css/images/productindex/1-3.png">
				<div class="second_sale">
					<p class="second_neme">你的名字  <span class="nt">NT 885</span></p>
					<p class="second_na">宮水三葉</p>
				</div>
			</li>
			<li class="second">		
				<img class="sccond_imgshow" src="css/images/productindex/1-4.png">
				<div class="second_sale">
					<p class="second_neme">請問您今..？  <span class="nt">NT 1138</span></p>
					<p class="second_na">心愛</p>
				</div>
			</li>
		</ul>

			<ul class="second_pic ">
				<li class="second ">
					<img class="sccond_imgshow" src="css/images/productindex/2.png">
					<div class="second_sale">
						<p class="second_neme">YURI ! on ICE<span class="nt">NT 1138</span></p>
						<p class="second_na">普里榭茨基</p>
					</div>
				</li>
				<li class="second">
					<img class="sccond_imgshow" src="css/images/productindex/2-1.png">
					<div class="second_sale">
						<p class="second_neme">鋼之鍊金術師  <span class="nt">NT 1330</span></p>
						<p class="second_na">愛德</p>
					</div>				
				</li>
				<li class="second">
					<img class="sccond_imgshow" src="css/images/productindex/2-2.png">
					<div class="second_sale">
						<p class="second_neme">遊戲人生   <span class="nt">NT 1214</span></p>
						<p class="second_na">吉普莉爾</p>
					</div>
				</li>
				<li class="second">
					<img class="sccond_imgshow" src="css/images/productindex/2-3.png">
					<div class="second_sale">
						<p class="second_neme">精靈寶可夢 <span class="nt">NT 1049</span></p>
						<p class="second_na">小智＆皮卡丘</p>
					</div>
				</li>
			</ul>
<div class="secone_new"><span class="font_one">7</span>月</div>
		<ul class="second_pic botton">
			<li class="second">	
				<img class="sccond_imgshow" src="css/images/productindex/3-1.png">
				<div class="second_sale">
					<p class="second_neme">請問您今..？  <span class="nt">NT 2198</span></p>
					<p class="second_na">智乃</p>
				</div>
			</li>
			<li class="second">
				<img class="sccond_imgshow" src="css/images/productindex/3-2.png">
				<div class="second_sale">
					<p class="second_neme">艦隊Collection  <span class="nt">NT 1467</span></p>
					<p class="second_na">清霜</p>
				</div>			
			</li>
			<li class="second">
				<img class="sccond_imgshow" src="css/images/productindex/3-3.png">
				<div class="second_sale">
					<p class="second_neme">精靈寶可夢  <span class="nt">NT 1380</span></p>
					<p class="second_na"> 辛西婭</p>
				</div>
			</li>
			<li class="second">
				<img class="sccond_imgshow" src="css/images/productindex/4-1.png">
				<div class="second_sale">
					<p class="second_neme">情色漫畫老師  <span class="nt">NT 1062</span></p>
					<p class="second_na">和泉紗霧</p>
				</div>
			</li>
		</ul>

		<ul class="second_pic last">
			<li class="second">
				<img class="sccond_imgshow" src="css/images/productindex/4-2.png">
				<div class="second_sale">
					<p class="second_neme">鎖鏈戰記<span class="nt">NT 1315</span></p>
					<p class="second_na"> 尤莉安娜</p>
				</div>
			</li>
			<li class="second">
				<img class="sccond_imgshow" src="css/images/productindex/4-3.png">
				<div class="second_sale">
					<p class="second_neme">LoveLive!<span class="nt">NT 885</span></p>
					<p class="second_na">國木田花丸</p>
				</div>				
			</li>
			<li class="second">
				<img class="sccond_imgshow" src="css/images/productindex/4-4.png">
				<div class="second_sale">
					<p class="second_neme">精靈寶可夢<span class="nt">NT 1229</span></p>
					<p class="second_na">Red</p>
				</div>
			</li>
			<li class="second">
				<img class="sccond_imgshow" src="css/images/productindex/4-5.png">
				<div class="second_sale">
					<p class="second_neme">血界戰線<span class="nt">NT 1214</span></p>
					<p class="second_na">雷歐納魯德·渥奇</p>
				</div>
			</li>
		</ul>
<?php include __DIR__. "/__page_footer.php" ?>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript" src="slick/slick.min.js"></script>
<script src="http://cdn.tutorialzine.com/misc/enhance/v3.js" async></script>

<script type="text/javascript">
	// $(document).ready(function(){
	//   $('.second_pic').slick({
	//    dots: true,
	//    infinite: true,
 //  	   slidesToShow: 3,
 // 	   slidesToScroll: 1,
	//   });
	// });
</script>
</body>

</html>