<header>
    <div class="header">
        <nav class="navbar">
            <div class="bar">
                <div class="hamburger">
                    <div class="ham_bar top_bar"></div>
                    <div class="ham_bar mid_bar"></div>
                    <div class="ham_bar mid_bar2"></div>
                    <div class="ham_bar bot_bar"></div>
                </div>
                <div class="logo">
                    <a href="index1.php"><img src="css/images/logo2.png" alt="LOGO"></a>
                </div>
                <ul class="menu">
                    <li class="<?= $pname=='product_list' ? 'active' : '' ?>"><a href="product_list.php">商品情報</a></li>
                    <li class="<?= $pname=='letest_news' ? 'active' : '' ?>"><a href="letest_news.php">最新消息</a>
                        <ul class="news_submenu">
                            <li ><a id="hovercolor" href="letest_news.php">所有消息</a></li>
                            <li><a  id="hovercolor" href="new.php">相關新聞</a></li>
                            <li><a  id="hovercolor" href="#">最新預購</a></li>
                            <li><a  id="hovercolor" href="#">活動優惠</a></li>
                        </ul>
                    </li>
                    <li class="<?= $pname=='shop' ? 'active' : '' ?>"><a href="shop.php">店鋪資訊</a></li>
                    <li class="<?= $pname=='secondhand' ? 'active' : '' ?>"><a href="secondhand.php">二手販賣</a>
                        <ul class="news_submenu4">
                            <li><a id="hovercolor" href="process_rule.php">販賣同意書</a></li>
                            <li><a id="hovercolor" href="Process.php">流程介紹</a></li>
                        </ul></li>
                    <ul class="header_right">
                        <li class="<?= $pname=='cart_list' ? 'active' : '' ?>">
                            <a class="cart" href="cart_list.php"><img src="css/images/cart_swrite.png" alt=""><span class="badge t-badge"></span></a>
                        </li>

                        <?php if(isset($_SESSION['user'])): ?>
                            <li class="<?= $pname=='register' ? 'active' : '' ?>">
                                <a class="register" href="member_login.php"><img src="css/images/loginwrite.png" alt=""></a>
                            </li>
                        <?php else: ?>
                            <li class="<?= $pname=='register' ? 'active' : '' ?>">
                                <a class="register" href="register.php"><img src="css/images/loginwrite.png" alt=""></a>
                            </li>
                        <?php endif; ?>

                        <?php if(isset($_SESSION['user'])): ?>
                            <li class=""><a  href="javascript: void(0)" style="margin-right: 0;">Hi, <?= $_SESSION['user']['name'] ?> ! </a> <span style="color: #fff;">|</span> </li>
                            <li><a href="logout.php">登出</a></li>
                        <?php endif; ?>
                    </ul>
                </ul>

                <ul class="sm_header_right">
                    <li>
                        <a class="cart" href="cart_list.php"><img src="css/images/cart_swrite.png" alt=""></a>
                    </li>
                    <li>
                        <a class="member" href="member.php"><img src="css/images/loginwrite.png" alt=""></a>
                    </li>
                </ul>
            </div>
            <nav class="sm_nav">
                <a href="product_list.php">商品情報
                    <div class="botBar"></div>
                </a>
                <a href="letest_news.php">最新消息
                    <div class="botBar"></div>
                </a>
                <a href="shop.php">店鋪資訊
                    <div class="botBar"></div>
                </a>
                <a href="secondhand.php">二手販賣
                    <div class="botBar"></div>
                </a>
            </nav>  
        </nav>
    </div>
</header>

<script>
// ----------------------------------------------------------------header_漢堡包
    var hamburger = $(".hamburger");
  
    hamburger.click(function(){
        var top_bar = $(".top_bar")
        var bot_bar = $(".bot_bar")
        var mid_bar = $(".mid_bar")
        var mid_bar2 = $(".mid_bar2")
        var sm_nav = $(".sm_nav")
        var sm_nav_a_1 = $(".sm_nav a:nth-child(1)")
        var sm_nav_a_2 = $(".sm_nav a:nth-child(2)")
        var sm_nav_a_3 = $(".sm_nav a:nth-child(3)")
        var sm_nav_a_4 = $(".sm_nav a:nth-child(4)")
        var sm_nav_botBar = $(".sm_nav a .botBar")
        top_bar.toggleClass("top_bar_active");
        bot_bar.toggleClass("bot_bar_active");
        mid_bar.toggleClass("mid_bar_active");
        mid_bar2.toggleClass("mid_bar2_active");
        sm_nav.toggleClass("sm_nav_active");
        sm_nav_a_1.toggleClass("active1");
        sm_nav_a_2.toggleClass("active2");
        sm_nav_a_3.toggleClass("active3");
        sm_nav_a_4.toggleClass("active4");
        sm_nav_botBar.toggleClass("active");
    });
//---------------------------------------------------header伸縮
    $(document).ready(function(){
        $(window).scroll(function(){
            var header = $("header")
            var scrollTop = $(this).scrollTop();
            var logo_img = $(".navbar .logo img");
            var navbar = $(".navbar");
            var header_width = $(".header").width();
            var navbar_menu = $(".navbar .menu");
            var navbar_bar = $(".navbar .bar");
            var st = scrollTop;

            if(st >= 200 && header_width > 767){
                logo_img.addClass("small_bar");
                navbar.addClass("small_bar");
                navbar_menu.addClass("small_bar");
                navbar_bar.addClass("small_bar");
                // header.addClass("small_bar");
            }else{
                logo_img.removeClass("small_bar");
                navbar.removeClass("small_bar");
                navbar_menu.removeClass("small_bar");
                navbar_bar.removeClass("small_bar");
                header.removeClass("small_bar");
            }
            console.log(scrollTop);
        });
    });
//-----------------------------------------------------------header外框
    
    $.get('add_to_cart.php', function(data){
        calItems(data);
    }, 'json');

    function calItems(obj){
        var total = 0;
        for(var s in obj){
            total += obj[s];
        }

        $('.t-badge').text( total);
    }


</script>