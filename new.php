<?php
require __DIR__ . '/__connect_db.php';
$pname = 'new';
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="css/reset.css">
<link rel="stylesheet" type="text/css" href="css/news.css">
 <?php include __DIR__. "/__page_head.php" ?>
	
		<title>新聞內頁</title>
</head>
<body>
	<?php include __DIR__. '/__page_header.php' ?>
	<div class="new_content">
		<div>
			<ul class="new_tag">
				<div class="new_bline">
					<a class="color" href="letest_news.php"><li>所有消息</li></a>
					<a class="color" href="#"><li>相關新聞</li></a>
				</div>	
				<div class="new_bline">
					<a class="color" href="#"><li>最新預購</li></a>
					<a class="color" href="#"><li>活動優惠</li></a>
				</div>
			</ul>
		</div>		
		<div class="new_titletwo">
			<div class="new_title">
				<!-- <div class="new_circle">
					<div class="new_big"><img src="images/tag/new_tagtitle.png"></div>
					<p class="new_time"></p>
				</div> -->
			</div>
			<div class="title_wo">
				<p class="title_word">
				<span class="fontfamily">Poppin'Party</span>鼓手「 山吹沙綾」，出自TV動畫
				『BanG Dream!』，於<span class="fontfamily">6</span>月<span class="fontfamily">29</span>日預購開跑！
				</p>
			</div>
		</div>	
		<div class="new_text colora">
			<p class="new_text_word">出自<span class="fontfamily">TV</span>動畫<span class="fontfamily">『BanG Dream!』</span>，<span class="fontfamily">「Poppin'Party」</span>中的手「 山吹沙綾 」登場！<br>表情零件備有「普通臉」、充滿元氣的「笑臉」與「滿足臉」。除了內附印有BanG Dream Logo的「爵士鼔」，可重現演唱會場景之外，搭配「踏板袋」還能賞玩出她在練習結束後準備回家的模樣。與另售的<span class="fontfamily">Poppin'Party</span>成員一同擺飾，賞玩各式各樣的情境吧！</p>
		</div><!-- new_text -->	
		<div class="new_images">
			<img class="oneimage" src="css/images/NewIP/news_1.png">
			<div class="new_right">
				<img class="twoimage" src="css/images/NewIP/news_2.png">
				<div class="new_bgsmail">
					<p class="new_one">線上商店特典 </p>
					<p class="new_two">山吹沙綾 爵士鼓</p>
					<br>
					<div class="new_content_five">
						商品名：二頭身 山吹沙綾<br>
						尺吋：約<span class="fontfamily">100mm </span><br>
						原型製作：七兵衛<br>
						價格：<span class="fontfamily">NT1543</span><br>
						發售日：<span class="fontfamily">2017/12 </span>
					</div>
					<div class="new_button">
						<a href=""><div class="new_buy">我要買</div></a>
						<a href=""><div class="new_track">追蹤</div></a>
						<a href=""><img class="imgicon" src="css/images/icon_share.png"></a>
					</div>
				</div><!-- new_bgsmail -->
			</div><!-- new_right -->
		</div><!-- new_image -->
	</div><!-- new_content -->

	<div class="new_content">
		<div class="new_title_two">
			<p class="new_other">其他新聞</p>
		</div><!-- new_title -->
		<!-- 圖片開始 -->
		<div class="new_images">
			<div class="new_picture">
				<a href="">
					<ul class="new_otherone">
						<li class="new_picfour"><img src="css/images/NewIP/01.png">
						<p class="new_textone">櫻花任務 木春由乃 7月6日預購開跑</p></li>
					</ul>
				</a>
				<a href="">
					<ul class="new_otherone">
						<li class="new_picfour"><img src="css/images/NewIP/02.png">
						<p class="new_textone">刀劍亂舞-<span class="fontfamily">ONLINE</span>-鶯丸 7月7日預購開跑</li>
					</ul>
				</a>
				<a href="">
					<ul class="new_otherone">
						<li class="new_picfour"><img src="css/images/NewIP/04.jpg">
						<p class="new_textone">冰雪奇緣 艾沙 9月預購開跑</li>
					</ul>
				</a>
				<a href="">
					<ul class="new_otherone">
						<li class="new_picfour"><img src="css/images/NewIP/03.jpg">
						<p class="new_textone">幼女戰記  譚雅‧提古雷查夫  6月27日開跑</li>
					</ul>
				</a>
			<!-- 圖片結束 -->
			</div>
		</div>
	</div>
	<div class="new_content">
		<!-- <div class="new_title_two">
					<p class="new_other">我要留言</p>
				</div> --><!-- new_title -->

	<!-- <ul class="new_images">
	<div class="new_message">
		<li class="border_border"><img class="imgone" src="css/images/NewIP/Avatar2.png">
			<input  class="border_line" type="text" placeholder="你在想什麼?" ></input></li></div>
			<br>
			<li class="new_check_send"><div class="new_check">送出<div></li>
		<li class="border_border"><img class="imgone" src="images/NewIP/Avatar2.png">
			<div class="border_line"></div></li>
		<li class="border_border"><img class="imgone" src="images/NewIP/Avatar3.png">
			<div class="border_line"></div></li>
	</ul> -->

	</div>
 <?php include __DIR__. "/__page_foot.php" ?>
</body>
</html>
