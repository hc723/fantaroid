<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Login, Sign up Header</title>

	
<style>
body{
	height: 3000px;
	}
header{
	position: fixed;
	top:0;
	left:8.875vw;
	right:8.875vw;
	min-width: 1197px;
	background-color:#292c2f;
	box-shadow:0 2px 2px #ccc;
	box-sizing: border-box;
	z-index: 0;
	transition: all .55s;
}
		
.navbar{
	background: #ABABAB;
	max-width: 1600px;
	min-width: 100%;
	width:100%;
	font:32px Arial, Helvetica, sans-serif, "微軟正黑體","新細明體","細明體";
	padding: 0;
	margin:0 auto;
	display: block;
	height: 120px;
	/*text-align: center;*/
}


/* Logo */

.navbar  .logo{
	vertical-align: middle;	
	display: inline-block;
	cursor: pointer;
	margin-left: 8.875%;
	float:left;

}
.hamburger{
	display: none;
}
/* The navigation links */

.navbar a , .sm_nav a{
	display: inline-block;
	text-decoration:none;
	margin:0;
	padding: 0;
	margin-right:40px;
	line-height: 32px;
	min-width: auto;
	color: #F7F7F7;
}
.navbar .bar{
	width: 100%;
	height: 100%;
	display: block;
	padding-top: 20px;
	margin-bottom: -20px;
}
.navbar .menu{
	display: inline-block;
	margin:20px 8.875% 0 0;
	float:right;
}

#menu li:nth-child(4) a {
	margin-right:0px;
}
.navbar  nav a:hover {
	/*opacity: 1;*/
}

.navbar  nav a.selected {
	/*color: #608bd2;*/
}

/* Login/Sign up buttons */
.navbar  .menu, .navbar .menu li, .header_right,.header_right li{
	list-style: none;
	display: inline-block;
	min-width: auto;
}
.header_right{
	
}
.header_right .cart{

}
.header_right .member{
	margin-right: 0px;
}
.header_right li{
	
}
.header_right li img{
	margin-bottom: -2px;
}
.header_right .cart{
	margin-right: 35px;
}
.member img{
	/*margin-left: 35px;*/
}

.header-login-signup  ul li:hover{
	
}
.sm_nav{
	display: none;
}
.sm_header_right{
	display: none;
}

/* The sign up button */


/* Making the header responsive */
@media screen and (max-width:1600px) {
	header{
	position: fixed;
	top:0;
	left:0vw;
	right:0vw;
	max-width: 1600px;
	min-width: ;
	background-color:#292c2f;
	box-shadow:0 2px 2px #ccc;
	box-sizing: border-box;
	z-index: 0;
	}
}
/*-----------------------------------------------------------------@ media*/
@media screen and (max-width:767px) {
	.hamburger{
		width:50px;
		height:50px;
		background: ;
		display:block;
		float:left;
		margin:5px 0 0 6.51890482398957%;
		cursor: pointer;
	}
	.ham_bar{
		width:40px;
		height: 5px;
		background: #494949;
		margin:0 auto;
		transition: all .4s cubic-bezier(.44,.33,.39,.96);
	}
	.top_bar{
		margin-top: 7px;
	}
	.top_bar_active{
		transform: translateY(10px) rotate(45deg);
		opacity: 0;
	}
	.mid_bar{
		margin:10px auto;
	}
	.mid_bar_active{
		transform: rotate(45deg);
	}
	.mid_bar2{
		margin:-15px auto 10px;
	}
	.mid_bar2_active{
		transform: rotate(-45deg);
	}
	.bot_bar_active{
		transform: translateY(-10px) rotate(315deg);
		opacity: 0;
	}
	header{
		position: fixed;
		top:0;
		left:0vw;
		right:0vw;
		max-width: 767px;
		min-width: 300px;
		background-color:#292c2f;
		box-shadow:0 2px 2px #ccc;
		box-sizing: border-box;
		z-index: 0;

	}
	header .navbar {
		width: 100vw;
		background: #ABABAB;
		max-width: 767px;
		width:100%;
		font:32px Arial, Helvetica, sans-serif, "微軟正黑體","新細明體","細明體";
		padding: 0;
		margin:0 auto;
		display: block;
		height: 100px;
	/*text-align: center;*/
	}
	header .bar{
		width:100vw;
		text-align: center;
	}
	.navbar .menu{
		display: none;
		width: 0;
	}
	.navbar  .logo{
		vertical-align: middle;	
		display: inline-block;
		cursor: pointer;
		float: none;

	}
	.navbar  .logo img{
		height: 60px;
	}
	.sm_header_right{
		display: inline-block;
		list-style: none;
		float: right;
		margin-top: 14px;
	}
	.sm_header_right li{
		float: left;
	}
	.sm_nav{
		position: fixed;
		right:0;
		left:0;
		display: block;
		background: rgba(182,182,182,0);
		animation: all .75s;
		font:24px Arial, Helvetica, sans-serif, "微軟正黑體","新細明體","細明體";
		box-sizing: border-box;
		box-shadow:0 0px 0px #ccc;
		transform: translateY(-330px);
		z-index: -1;
		transition: all .65s cubic-bezier(.44,.33,.36,1.06);
	}
	.sm_nav_active{
		transform: translateY(0px);
		background: rgba(182,182,182,0.6);
		box-shadow:0 2px 1px #ccc;
	}
	.sm_nav a{
		color: #3A3F46;
		font-size: 24px;
		display: block;
		margin:0 auto;
		width: 100%;
		text-align: center;
		padding:20px 0px;
		transition: all .5s cubic-bezier(.44,.33,.36,1.06);
		transform: translateX(-500px);
	}
	@keyframes slideRight {
     from {
     	transform: translateX(-500px)
     }
     to {
     	transform: translateX(0px)
     }
    }

	.sm_nav .active1{
		animation: slideRight .45s forwards ;
		animation-delay: 0.23s;
	}
	.sm_nav .active2{
		animation: slideRight .45s forwards ;
		animation-delay: 0.27s;
	}
	.sm_nav .active3{
		animation: slideRight .45s forwards ;
		animation-delay: 0.31s;
	}
	.sm_nav .active4{
		animation: slideRight .45s forwards ;
		animation-delay: 0.35s;
	}
	.sm_header_right li{
		margin-right: 30px;
	}
	.sm_header_right li:nth-child(2){
		margin-right: 20px;
	}
	.sm_header_right a{
		margin:0;
	}
	.sm_nav a.active{
		opacity: 1;
	}
	.sm_nav .sm_nav_first{
		padding-top:40px;
	}
	.sm_nav .sm_nav_last{
		padding-bottom:40px;
	}
}

/* For the headers to look good, be sure to reset the margin and padding of the body */

body, ul, li {
	margin:0;
	padding:0;
}
</style>
</head>

<body>
	<header>
		<nav class="navbar">
			<div class="bar">
				<div class="hamburger">
					<div class="ham_bar top_bar"></div>
					<div class="ham_bar mid_bar"></div>
					<div class="ham_bar mid_bar2"></div>
					<div class="ham_bar bot_bar"></div>
				</div>
				<div class="logo">
					<img src="images/logo.png" alt="">
				</div>
				<ul class="menu">
					<li><a href="#">商品情報</a></li>
					<li><a href="#">最新消息</a></li>
					<li><a href="#">店鋪資訊</a></li>
					<li><a href="#">二手販賣</a></li>
					<ul class="header_right">
						<li>
							<a class="cart" href=""><img src="images/cart_s.png" alt=""></a>
						</li>
						<li>
							<a class="member" href=""><img src="images/loginw_s.png" alt=""></a>
						</li>
					</ul>
				</ul>

				<ul class="sm_header_right">
						<li>
							<a class="cart" href=""><img src="images/cart_s.png" alt=""></a>
						</li>
						<li>
							<a class="member" href=""><img src="images/loginw_s.png" alt=""></a>
						</li>
				</ul>
			</div>
			<nav class="sm_nav">
				<a href="#">商品情報</a>
				<a href="#">最新消息</a>
				<a href="#">店鋪資訊</a>
				<a href="#">二手販賣</a>
			</nav>	
		</nav>
</header>



<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="http://cdn.tutorialzine.com/misc/enhance/v3.js" async></script>
<script
  src="https://code.jquery.com/jquery-3.2.1.js"
  integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
  crossorigin="anonymous">
  </script>

  <script>
  // ----------------------------------------------------------------漢堡包
    	var hamburger = $(".hamburger");
    	
    	hamburger.click(function(){
    		var top_bar = $(".top_bar")
    		var bot_bar = $(".bot_bar")
    		var mid_bar = $(".mid_bar")
    		var mid_bar2 = $(".mid_bar2")
    		var sm_nav = $(".sm_nav")
    		var sm_nav_a_1 = $(".sm_nav a:nth-child(1)")
    		var sm_nav_a_2 = $(".sm_nav a:nth-child(2)")
    		var sm_nav_a_3 = $(".sm_nav a:nth-child(3)")
    		var sm_nav_a_4 = $(".sm_nav a:nth-child(4)")
			top_bar.toggleClass("top_bar_active");
			bot_bar.toggleClass("bot_bar_active");
			mid_bar.toggleClass("mid_bar_active");
			mid_bar2.toggleClass("mid_bar2_active");
			sm_nav.toggleClass("sm_nav_active");
			sm_nav_a_1.toggleClass("active1");
			sm_nav_a_2.toggleClass("active2");
			sm_nav_a_3.toggleClass("active3");
			sm_nav_a_4.toggleClass("active4");
	});

	
  </script>
</body>

</html>

