<!DOCTYPE html>
<html lang="en">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="css/reset.css">
<link rel="stylesheet" type="text/css" href="css/letest_news.css">
<?php include __DIR__. "/__page_head.php" ?>
<head>
    <?php include __DIR__. "/__page_header.php" ?>
    <meta charset="UTF-8">
    <title>最新消息</title>
    <style type="text/css">

    </style>
</head>
<body>

<!-- <div class="new_latest">
 <ul class="new_tag">
 <div class="new_bline">
   <a class="color" href="#"><li>所有消息</li></a>
   <a class="color" href="new.php"><li>相關新聞</li></a>
  </div>
  <div class="new_bline">
   <a class="color" href="#"><li>最新預購</li></a>
   <a class="color" href="#"><li>活動優惠</li></a>
  </div>
 </ul>
</div>   -->
<!-- <div class="new_title">
 <p class="all_latest">所有消息</p>
</div> -->
<div class="letest_all">
    <div class="letest">
        <div class="imgsrc srcpng">
            <img class="letest_img" src="css/images/letest/01.png">
        </div>
        <div class="opentime">
            <div class="kind">新聞</div>
            <div class="dateday">2017/06</div>
            <div class="time">18</div>
        </div><!-- opentime -->
        <div class="pic1">

            <p class="narrative">你最安心信賴的好鄰居！「蜘蛛人」返校日！</p>
            <p class="letest_concnet">來自最新電影《蜘蛛人：返校日》的「 蜘蛛人 返校日 Edition」預計於 2017 年 11 月發售的消息，在這個版本中，鄰家英雄蜘蛛人將加入漫威電影宇宙，彼得·帕克接受恩師「鋼鐵人」東尼．史塔克的協助。</p>
        </div><!-- pic1 -->

    </div>
    <div class="letest">
        <div class="imgsrc srcpng">
            <img class="letest_img" src="css/images/letest/02.png">
        </div>
        <div class="opentime">
            <div class="kind">最新預購</div>
            <div class="dateday">2017/07</div>
            <div class="time">02</div>
        </div><!-- opentime -->
        <div class="pic1">
            <p class="narrative">鋼之鍊金術師「愛德華·艾力克」６月27日預購開跑！</p>
            <p class="letest_concnet">愛德華．艾力克是史上最年輕的國家鍊金術師，被賦予「鋼」的稱號，加上增高鞋和頭上呆毛後身高 165 公分（自稱）。天才般的愛德擁有豐富的鍊金術知識與技術，特別擅長金屬系的鍊金術。同時也因為看過「真理之門」，使得他不須畫出鍊成陣！</p>
        </div><!-- pic1 -->

    </div>
    <div class="letest">
        <div class="imgsrc srcpng">
            <img class="letest_img" src="css/images/letest/03.png">
        </div>
        <div class="opentime">
            <div class="kind">最新預購</div>
            <div class="dateday">2017/07</div>
            <div class="time">15</div>
        </div><!-- opentime -->
        <div class="pic1">

            <p class="narrative">「鋼之鍊金術師」「阿爾馮斯·愛力克」 監修中！</p>
            <p class="letest_concnet">近日於官網繼續公佈了弟弟「阿爾馮斯?愛力克」的監修中情報照片。
                只曝光試作彩色樣品的照片，就令喜愛《鋼之鍊金術師》的人們相當期待！以金屬塗裝來呈現阿爾的靈魂固定的盔甲上面，到時候當然要一起與哥哥「愛德華」共同購入的....
            </p>
        </div><!-- pic1 -->

    </div>
    <div class="letest">
        <div class="imgsrc srcpng">
            <img class="letest_img" src="css/images/letest/04.png">
        </div>
        <div class="opentime">
            <div class="kind">新聞</div>
            <div class="dateday">2017/07</div>
            <div class="time">19</div>
        </div><!-- opentime -->
        <div class="pic1">
            <p class="narrative">《火影忍者》 春野櫻 預定2017年7月發售！</p>
            <p class="letest_concnet">儘管新世代的慕留人成為目前焦點，老爸世代帶來的感動依然不會消失！
                在「漩渦鳴人」、「宇智波佐助」、「旗木卡卡西」等人相繼黏土人化後，「春野櫻」、「宇智波鼬」等人也決定參戰！
            </p>
        </div><!-- pic1 -->

    </div>
    <div class="letest">
        <div class="imgsrc srcpng">
            <img class="letest_img" src="css/images/letest/05.png">
        </div>
        <div class="opentime">
            <div class="kind">新聞</div>
            <div class="dateday">2017/07</div>
            <div class="time">25</div>
        </div><!-- opentime -->
        <div class="pic1">
            <p class="narrative">獻出心臟！！ 「進擊的巨人」艾爾文‧史密斯！</p>
            <p class="letest_concnet">來自《進擊的巨人》的「黏土人 艾爾文‧史密斯」預計於 2018 年 01 月發售的消息，「艾爾文‧史密斯」是調查兵團第 13 任團長，擁有過人的深遠思慮。為了達成目的，連曾是地痞的「里維·阿克曼」、擁有巨人化能力被視為危險的「艾連．葉卡」都收編至兵團內！</p>
        </div><!-- pic1 -->

    </div>
</div><!-- latest_all -->
</div><!-- new_latest -->
<?php include __DIR__. "/__page_foot.php" ?>
<script type="text/javascript">
    $(document).ready(function(){
        $(".letest").hide();
        $(".letest:eq(0)").fadeIn(3000);
        $(".letest:eq(1)").fadeIn(5000);
        $(".letest:eq(2)").fadeIn(7000);
        $(".letest:eq(3)").fadeIn(10000);
        // $(".letest:eq(4)").fadeIn(13000);
        $(window).scroll(function() {
            console.log($("body").scrollTop());
            $scrollTop=$("body").scrollTop();
            if($scrollTop>=70){
                $(".letest:eq(4)").fadeIn(3000);
            }
        });
    });
</script>
</body>
</html>