<?php
require __DIR__ . '/__connect_db.php';
$pname = 'history';

if(!isset($_SESSION['user'])){
    exit;
}

$t = time() - 180*24*60*60;
$t2 = date('Y-m-d', $t);
/*
$sql = sprintf("SELECT * FROM `orders` WHERE `member_sid`=%s AND `order_date` > '%s' ORDER BY `order_date` DESC",
        $_SESSION['user']['id'],
        $t2
    );
*/

$sql = sprintf("SELECT o.`amount`, o.`order_date`,
  d.`order_sid`, d.`product_sid`, d.`price`, d.`quantity`,
  p.`bookname`
FROM `orders` o
JOIN `order_details` d ON o.sid=d.order_sid
JOIN `products` p ON d.product_sid=p.sid
WHERE o.`member_sid`=%s AND o.`order_date` > '%s'
ORDER BY o.`order_date` DESC;",
    $_SESSION['user']['id'],
    $t2
    );
//echo $sql;
//exit;
$rs = $mysqli->query($sql);

$data = array();

$order_sid = 0;
$index = -1;
while($row = $rs->fetch_assoc()):
    if($order_sid != $row['order_sid']):
        $order_sid = $row['order_sid'];
        $index++;
        $data[$index] = array(
                'order_sid' => $row['order_sid'],
                'amount' => $row['amount'],
                'order_date' => $row['order_date'],
                //'details' => array(),
        );
    endif;
    $data[$index]['details'][] = array(
            'product_sid' => $row['product_sid'],
            'price' => $row['price'],
            'bookname' => $row['bookname'],
            'quantity' => $row['quantity'],
    );

endwhile;

//print_r($data);
//exit;

?>
<?php include __DIR__. '/__page_head.php' ?>

<div class="container">
    <?php include __DIR__. '/__navbar.php' ?>


        <div class="col-md-12">

            <table class="table table-striped">
                <thead>
                <tr>
                    <th>訂單編號</th>
                    <th>總價</th>
                    <th>訂購時間</th>
                    <th>細項</th>
                </tr>
                </thead>
                <tbody>

                <?php foreach($data as $o_item): ?>
                <tr>
                    <th><?= $o_item['order_sid'] ?></th>
                    <td><?= $o_item['amount'] ?></td>
                    <td><?= $o_item['order_date'] ?></td>
                    <td>
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>商品名稱</th>
                                <th>價格</th>
                                <th>數量</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php foreach($o_item['details'] as $d): ?>
                                    <tr>
                                        <td><?= $d['bookname'] ?></td>
                                        <td><?= $d['price'] ?></td>
                                        <td><?= $d['quantity'] ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <?php endforeach; ?>
                </tbody>
            </table>


        </div>



</div>
    <script>


    </script>
<?php include __DIR__. '/__page_foot.php' ?>