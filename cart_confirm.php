<?php
require __DIR__ . '/__connect_db.php';
$pname = 'cart_confirm';

if(empty($_SESSION['cart'])){
    $has_data = false;
} else {
    $keys = array_keys($_SESSION['cart']);

    $sql = sprintf("SELECT * FROM `products` WHERE `sid` IN (%s)", implode(',', $keys));

    $rs = $mysqli->query($sql);

    while($row = $rs->fetch_assoc()){
        $row['qty'] = $_SESSION['cart'][$row['sid']]; // 取得某項商品的數量
        $c_prod[ $row['sid'] ] = $row;
    }
    $has_data = true;
//print_r($c_prod);
//exit;
}

?>
    <style>
    
    body{
        padding: 0;
        margin: 0;
        box-sizing: border-box;
        background: #FBF9F5;
        font-family:"微軟正黑體";
    }
        .del {
            color: red;
            cursor: pointer;
        }
        .cart_listbox{
        max-width: 960px;
        margin: 0 auto;
        padding: 30px;
        }
        .text_pic{
        line-height: 40px;
        width: 1000px;
        padding: 50px;
        }
        /*.text_pic指定下面的td*/
    .text_pic td{
        width: 150px;
        vertical-align: middle;
        text-align: center;
        padding: 30px 0;
    }
    th.picflex{
        width: 160px;
    }
    .color{
        background: #FB9134;
        color:#fff;
    }
    a.btn{
    color:#fff;
    line-height: 40px;
}
    .smail{
        width: 100px;
        height: 40px;
        background: #FB8134;
        margin: 0 auto;
        cursor: pointer;
        text-align: center;
        font-size: 20px;
    }
    .alert.alert-info {
    text-align: center;
    padding: 10px;
}
.imgsrc{
        width: 165px;   
    }
    </style>
<?php include __DIR__. '/__page_head.php' ?>

<div class="container">
    <?php include __DIR__. '/__page_header.php' ?>


        <div class="cart_listbox">
            <?php if($has_data): ?>
            <table class="table table-striped">
                <thead>
                <tr class="text_pic color">
                    <th class="picflex">產品編號</th>
                    <th class="picflex">產品名稱</th>
                    <th class="picflex">圖片</th>
                    <th class="picflex">價格</th>
                    <th class="picflex">數量</th>
                    <th class="picflex">小計</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($c_prod as $item): ?>
                <tr class="text_pic" data-sid="<?= $item['sid'] ?>">
                    <td><?= $item['sid'] ?></td>
                    <td><?= $item['name'] ?></td>
                    <td><img class="imgsrc" src="<?= $item['img_id'] ?>" alt="<?= $item['name'] ?>"></td>
                    <td class="price" data-val="<?= $item['price'] ?>"></td>
                    <td class="qty"><?= $item['qty'] ?></td>
                    <td class="sub-total"></td>

                </tr>
                <?php endforeach; ?>

                </tbody>
            </table>
                
                <div class="alert alert-info" role="alert">總計: <strong id="amount"></strong></div>

                <?php if(isset($_SESSION['user'])): ?>
                    <div class="smail">
                    <a class="btn" href="buy.php">結帳</a></div>
                <?php else: ?>
                    <a class="btn" href="login.php">請先登入會員再結帳</a>
                <?php endif; ?>

            <?php else: ?>
                <div class="alert alert-danger" role="alert">購物車裡沒有商品</div>
            <?php endif; ?>


        </div>



</div>
    <script>
        var dallorCommas = function(n){
            return '$ ' + n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        };

        $('.sub-total').each(function(){
            var tr = $(this).closest('tr');
            var price_td = tr.find('.price');
            var price = price_td.attr('data-val');
            var qty = tr.find('.qty').text();
            price_td.text( dallorCommas( price ) );
            $(this).text( dallorCommas( price*qty ) );
        });

        // 計算總價
        var calTotalAmount = function(){
            var t = 0;
            $('.sub-total').each(function(){
                var tr = $(this).closest('tr');
                var price_td = tr.find('.price');
                var price = price_td.attr('data-val');
                var qty = tr.find('.qty').text();
                t += price*qty;
            });

            $('#amount').text( dallorCommas(t) );
        };
        calTotalAmount();
    </script>
<?php include __DIR__. '/__page_foot.php' ?>