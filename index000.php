<?php
require __DIR__ . '/__connect_db.php';
$pname = 'index';


?>
<?php include __DIR__. '/__page_head.php' ?>
<link type="text/css" rel="stylesheet" href="slick/slick/slick.css">
<link type="text/css" rel="stylesheet" href="slick/slick/slick-theme.css">
<link rel="stylesheet" type="text/css" href="css/index.css">
<style>
    body{
        overflow: hidden;/*-------------auto*/
    }
    .header{
        opacity: 0; /*-------------*/
        transform: translateY(-105px) ;/*-105px*/
    }
    .navbar .logo{
        opacity: 0;
    }
    footer {
        opacity: 1;
    }
    footer{
        clear: right;
    }
</style>

<div class="container">
    <?php include __DIR__. '/__page_header.php' ?>
    <div class="ani_frame">
        <div class="perspective">
            <div class="ani_logo">
                <div class="mask fa"></div>
                <div class="mask nt"></div>
                <div class="mask a"></div>
                <div class="mask r"></div>
                <div class="o_pers">
                    <div class="o"></div>
                </div>
                <div class="mask i"></div>
                <div class="mask d"></div>
            </div>
            <div class="skip">
                <span>skip</span>
            </div>
        </div>
    </div><!--ani_frame-->

    <div class="main">
        <div class="content">
            <div class="slider">
                <div class="sl_area_01">
                    <img src="css/images/index/slider1.png" alt="">
                </div>

                <div class="sl_area_02">
                    <img src="css/images/index/slider2.png" alt="">
                </div>

                <div class="sl_area_03">
                    <img src="css/images/index/slider3.png" alt="">
                </div>
            </div><!--slider-->

            <div class="main_link">
                <div class="arrow_dowm">
                        <img src="css/images/index/right.png" alt="">
                </div>
                <div class="pro_intel">
                    <div class="pro_intel_h head">商品情報</div>
                    <div class="pro_intel_area">
                        <div class="intel_pro_img proframe">
                            <img src="css/images/index/0.png" alt="">
                            <div class="intel_pro_text pro_text">
                                <p>Triad Primus</p>
                                <p>Kahasu</p>
                            </div>
                        </div>

                        <div class="pro_intel_txt text">
                            <h6>Triad Primus</h6>
                            <p>這是Triad Primus的Kahasu，這一天終於來了!<br>
                                而凜澀谷今年六月Kahasu中發布，即來到的一站，讓我們看看他們的可愛!!<br>
                                即將在七月正式發售!!<br>
                                想把她帶回家嗎?快來看看喔!!</p>
                        </div>
                    </div>

                    <div>
                        <div class="pro_intel_txt_L text">
                            <h6>Triad Primus</h6>
                            <p>我是Mobamasu，一起和kahasu站在舞台上。<br>
                                Triad Primus這款獨創的舞台服裝，看看這套簡單又美麗的制服，<br>
                                從七月十五號開始販售！?<br>
                                Triad Primus的最後一位成員，Nendoroid Karen Hojo 也將在即將上映！</p>
                        </div>
                        <div class="intel_pro_img_R proframe">
                            <img src="css/images/index/0-1.png" alt="">
                            <div class="intel_pro_text pro_text">
                                <p>Triad Primus</p>
                                <p>Mobamasu</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="sec_hand_pro">
                    <div class="sec_hand_pro_h head"> 二手商品</div>
                    <div>
                        <div class="sec_hand_pro_img proframe">
                            <img src="css/images/index/1.png" alt="">
                            <div class="sec_hand_pro_text pro_text">
                                <p>魔法少女</p>
                                <p>奈葉</p>
                            </div>
                        </div>

                        <div class="sec_hand_txt text">
                            <h6>魔法少女奈葉</h6>
                            <p>可愛Q版來囉!<br>
                                “全新的”魔法少女奈葉THE MOVIE 1<br>
                                佩蒂特葉和命運設定最後一幕版本!!<br>
                                良品枯寂溢價魔法少女抒情的電影第二號~趕快來蒐集!</p>

                        </div>
                    </div>

                    <div>
                        <div class="sec_hand_pro_text_L text">
                            <h6>魔法少女奈比</h6>
                            <p>2010 年1月冬季不知道節日限量版發佈!<br>
                                這是VITA 維塔白色騎士服，表情零件有充滿元氣的「笑臉」、<br>
                                「驚嚇臉」以及吃著饅頭的可愛「嚼嚼臉」，<br>
                                盡情欣賞在鄉下努力奮鬥的由乃黏土人吧！</p>
                        </div>
                        <div class="sec_hand_pro_img_R proframe">
                            <img src="css/images/index/0-1.png" alt="">
                            <div class="sec_hand_pro_text pro_text">
                                <p>魔法少女</p>
                                <p>奈比</p>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="Pre_order_pro">
                    <div class="Pre_order_pro_h head">預購商品</div>

                    <div class="Pre_order_area1">
                        <div class="Pre_order_img proframe">
                            <img src="css/images/index/2.png" alt="">
                            <div class="intel_pro_text pro_text">
                                <p>山吹荒培防</p>
                                <p>山吹沙綾</p>
                            </div>
                        </div>

                        <div class="Pre_order_txt text">
                            <h6>歡迎光臨山吹烘焙坊！</h6>
                            <p>出自TV動畫『BanG Dream!』，「Poppin'Party」<br>
                                中的的鼔手「山吹沙綾」變身成黏土人登場?<br>
                                表情零件備有「普通臉」、充滿元氣的「笑臉」與「滿足臉」。<br>
                                除了內附印有BanG Dream Logo的「爵士?」可重現演唱會場景之外，<br>
                                與另售的Poppin'Party成員一同擺飾，賞玩各式各樣的情境吧！<br>
                            </p>
                        </div>
                    </div>


                    <div class="Pre_order_area2">
                        <div class="Pre_order_txt_L text">
                            <h6>GSC《動物朋友》 藪貓 開放預購中!</h6>
                            <p>由 GSC 所推出的《動物朋友》「藪貓」，<br>
                                就在 7 月 28 日這天正式開放預購！<br>
                                商品的發售日期為 2017 年 10 月。?<br>
                                若於「GOODSMILE線上商店」訂購「藪貓」，將贈送「加帕利包子＋手掌零件」特典！
                            </p>
                        </div>
                        <div class="Pre_order_img_R proframe">
                            <img src="css/images/index/2-1.png" alt="">
                            <div class="intel_pro_text pro_text">
                                <p>動物朋友</p>
                                <p>藪貓</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div><!--content-->
    </div><!---main -->
</div>

<script type="text/javascript" src="script/jquery-migrate-1.2.1.js"></script>
<script type="text/javascript" src="slick/slick/slick.js"></script>
<script>
    /*-----------------------------------------變形中心點隨機*/
    var random_array = [];//--------------------儲存隨機6個數字陣列

    function random_origin(){
        $(".mask").each(function() {
            var random_1_100 = parseInt(Math.random() * 99 + 1); //每個mask隨機一次數字
            // console.log("random_1_100:" + random_1_100);
            var that = $(this);
            var ran_num = random_1_100 ;

//            console.log("ran_num:" + ran_num);
            if (0 < ran_num && ran_num <= 25) {
                /*-----------------------------------中心上-*/
                random_array.push(ran_num); //---------------把隨機數字丟到陣列
                    that.css({
                        "transition": "all 0s",
                        "transform-origin": "top",
                        "transform": "scale(1,0)"
                    });
//                that.html("<div>1~25,上</div><div>" + ran_num + "</div>");
            } else if (25 < ran_num && ran_num <= 50) {
                /*-----------------------------------中心下-*/
                random_array.push(ran_num);
                that.css({
                    "transition":"all 0s",
                    "transform-origin": "bottom",
                    "transform": "scale(1,0)"
                });
//                that.html("<div>26~50,下</div><div>" + ran_num + "</div>");

            } else if (50 < ran_num && ran_num <= 75) {
                /*-----------------------------------中心左-*/
                random_array.push(ran_num);
                that.css({
                    "transition":"all 0s",
                    "transform-origin": "left",
                    "transform": "scale(0,1)"
                });
//                that.html("<div>51~75,左</div><div>" + ran_num + "</div>");
            } else {
                /*-----------------------------------中心右-*/
                random_array.push(ran_num);
                that.css({
                    "transition":"all 0s",
                    "transform-origin": "right",
                    "transform": "scale(0,1)"
                });
//                that.html("<div>76~100,右</div><div>" + ran_num + "</div>");
            }
        });
// console.log("陣列:" + random_array);
    }

    function random_first_origin(){
        $(".mask").each(function() {
            var random_1_100 = parseInt(Math.random() * 99 + 1); //每個mask隨機一次數字
            // console.log("random_1_100:" + random_1_100);
            var that = $(this);
            var ran_num = random_1_100 ;

//            console.log("ran_num:" + ran_num);
            if (0 < ran_num && ran_num <= 25) {
                /*-----------------------------------中心上-*/
                random_array.push(ran_num); //---------------把隨機數字丟到陣列
                that.css({
                    "transition": "all 0s",
                    "transform-origin": "top",
                    "transform": "scale(1,1)"
                });
//                that.html("<div>1~25,上</div><div>" + ran_num + "</div>");
            } else if (25 < ran_num && ran_num <= 50) {
                /*-----------------------------------中心下-*/
                random_array.push(ran_num);
                that.css({
                    "transition":"all 0s",
                    "transform-origin": "bottom",
                    "transform": "scale(1,1)"
                });
//                that.html("<div>26~50,下</div><div>" + ran_num + "</div>");

            } else if (50 < ran_num && ran_num <= 75) {
                /*-----------------------------------中心左-*/
                random_array.push(ran_num);
                that.css({
                    "transition":"all 0s",
                    "transform-origin": "left",
                    "transform": "scale(1,1)"
                });
//                that.html("<div>51~75,左</div><div>" + ran_num + "</div>");
            } else {
                /*-----------------------------------中心右-*/
                random_array.push(ran_num);
                that.css({
                    "transition":"all 0s",
                    "transform-origin": "right",
                    "transform": "scale(1,1)"
                });
//                that.html("<div>76~100,右</div><div>" + ran_num + "</div>");
            }
        });
// console.log("陣列:" + random_array);
    }

    //---------------------------遮罩變短

    function mask_to_0(){
        $(".mask").each(function () {
            var get_num = random_array.shift();
            var that = $(this);

            if(0 < get_num && get_num <= 50){
//                console.log("抓出陣列的數字" + get_num);
                that.css({
                    "transform" : "scale(1,0)",
                    "transition" : "all .25s"
                });
            }else{
//                console.log("抓出陣列的數字" + get_num);
                that.css({
                    "transform" : "scale(0,1)",
                    "transition" : "all .25s"
                });
            }
        });
    };
    //---------------------------遮罩變長
    function mask_to_100() {
        $(".mask").each(function () {
            var get_num = random_array.shift();
            var that = $(this);
//            console.log("抓出陣列的數字" + get_num);
            that.css({
                "transform": "scale(1,1)",
                "transition": "all .25s"
            });

        });
    }
    /*-----------------------------------------遮罩變形動畫*/
    var mask_speed_fast = 250;
    function mask_animate_fa(){
            setTimeout( random_first_origin,250);
            setTimeout(mask_to_0,300);
            setTimeout(random_origin,600);
            setTimeout( mask_to_100,750);//1000
        }


    /*-----------------------------------------執行次數*/
    var connect_time = 0 ;
    var mask_ani_time = 1000 ;
    function mask_ani_times(times){
        for(i=0;i<times;i++){
            setTimeout(mask_animate_fa,mask_ani_time * i );
            connect_time +=1;
        }
    }

    /*--------------------------------------------------笑臉動畫*/
    /*------------往上動畫*/
    function o_ani_up(){

        var o_pers = $(".o_pers");
        o_pers.css({
            "transition":"all .4s cubic-bezier(.46,.08,.28,1.19) ",
            "transform":"translateY(0px) ",
            "opacity": "1"
        });
    };
    /*------------旋轉動畫*/
    function o_rotate(){
        var one_round = 360 ;
        var o_rotate = $(".o");
        o_rotate.css({
            "transition":"all .8s cubic-bezier(.46,.08,.28,1.19) ",
            "transform":"rotateY(" + one_round*1.5 + "deg)"
        });
    }
    /*--------------------------------------------------整張LOGO動畫*/
    /*-------------LOGO淡入 + 變小 */
    var logo = $(".ani_logo");
    function LOGO_ani_far(){
        logo.css({
            "transition":"all .8s",
            "transform": "scale(.3,.3)",
            "opacity": "1"
            //console.log("connect_time:" + connect_time);
        });
    }

    /*-------------LOGO飛起來 + 旋轉 */
    function LOGO_ani_fly() {
        logo.css({
            "transition": "all .3s cubic-bezier(.55,.54,.6,.61)",
            "transform": "scale(.6,.6) rotateX(25deg) rotateY(15deg) rotateZ(0deg) translate3d(900px,-500px,0px)"
        });
    };


    function LOGO_ani_fly2() {
        logo.css({
            "transform-origin": "50% 50%",
            "transition": "all .23s cubic-bezier(0,.74,.2,1.01) ",
            "transform": "scale(.65,.65) rotateX(40deg) rotateY(11deg) rotateZ(4deg) translateX(500px) translateY(-800px) translateZ(0px)"
        });
    };

    function LOGO_ani_down() {
        logo.css({
            "transition": "all .2s cubic-bezier(.98,0,.17,1.02)",
            "transform":"scale(0.1201, 0.1201) rotateX(0deg) rotateY(0deg) rotateZ(0deg) translateX(-6777px) translateY(-4403px) translateZ(-1000px)"
        });
    };
    function LOGO_ani_start_fly (){
        LOGO_ani_far();
        LOGO_ani_fly();
        setTimeout(LOGO_ani_fly2,350);
        setTimeout(LOGO_ani_down,500);
    }

    //-----------------------------header + main 進場
    //----------------------------動畫LOGO消失
    function logo_out() {
        var ani_frame = $(".ani_frame");
        ani_frame.css({
            "transition": "all 0s",
            "display": "none"
        })
        ani_frame.remove();
    }
    //----------------------------header,main進場,body捲軸
    function body_in() {
        var header = $(".header");
        header.css({
            "transition": "all .5s",
            "transform":"translateY(0px)",
            "opacity" : "1"
        });
        var main = $(".main");
        main.css({
            "transition": "all .5s",
            "transform":"translateY(0px)",
            "opacity" : "1"
        });
        var body = $("body");
        body.css({
            "overflow" : "auto"
        });

        setTimeout(nav_bar_logo_in,800);


        function nav_bar_logo_in(){
            var nav_bar_logo =  $(".navbar .logo");
            nav_bar_logo.css({
                "opacity" : "1"
            });
        }
    }
    function nav_bar_logo_in(){
        var nav_bar_logo =  $(".navbar .logo");
        nav_bar_logo.css({
            "opacity" : "1"
        });
    }
    //----------------------------header,body回位置
    function body_set(){
        var header = $(".header");
        var main = $(".main");

        header.css({
            "transform": " translateX(0px) translateY(0px) "
        });
        main.css({
            "transform": " translateX(0px) translateY(0px) "
        });
    }


    /*--------------------------------------------------串動畫*/

    var stid, stids = [];
    function open_ani(){
        LOGO_ani_far();
        mask_ani_times(2);
        stid = setTimeout( random_first_origin, mask_ani_time * connect_time+200);
        stids.push(stid);
        stid = setTimeout(mask_to_0, mask_ani_time * connect_time + 300);
        stids.push(stid);
        stid = setTimeout(o_ani_up, mask_ani_time * connect_time);
        stids.push(stid);
        stid = setTimeout(o_rotate, mask_ani_time * connect_time + 100);
        stids.push(stid);
        stid = setTimeout(LOGO_ani_start_fly, 3300);
        stids.push(stid);
        stid = setTimeout(body_in,3770);//4000
        stids.push(stid);
        stid = setTimeout(shake_times,4435);
        stids.push(stid);
        stid = setTimeout(logo_out,4480);
        stids.push(stid);
        stid = setTimeout(body_set, 5300);
        stids.push(stid);

    }
    //-------------------------------------跳過動畫 !!!!!問老師!!!!!!!!!!!!!
    //解決: 動畫用一個變數存, 依次push到陣列,

    $(".skip").click(function(){
        var ani_frame =  $(".ani_frame");
        ani_frame.css("display","none");
        body_in();
        nav_bar_logo_in();
        for(var i=0; i<stids.length; i++){
            clearTimeout(stids[i]);
        }

    });

    var window_width = $(window).width();
    //---------------------------------------------------------------動畫開關
    if( window_width >767 ) {
        $(document).ready(function () {
            open_ani();
        });
    }
    //----------------------------------------------畫面震動
    var shack_px = 36 ;
    function shake_ani(){
        var random_1_4 = parseInt(Math.random() * 4 + 1);
//        console.log(random_1_4);
        var shack_time = 0.02 ;

        if(random_1_4 === 1){
            $(".header").css({
                "transition": "all " + shack_time + "s ",
                "transform": "  translateX(" + shack_px + "px) translateY(" + shack_px + "px)"
            });
            $(".main").css({
                "transition": "all " + shack_time + "s ",
                "transform": "  translateX(" + shack_px + "px) translateY(" + shack_px + "px)"
            });
        }else if(random_1_4 === 2){
            $(".header").css({
                "transition": "all " + shack_time + "s ",
                "transform": "  translateX(" + "-" + shack_px + "px) translateY(" + shack_px + "px)"
            });
            $(".main").css({
                "transition": "all " + shack_time + "s ",
                "transform": "  translateX(" + "-" + shack_px + "px) translateY(" + shack_px + "px)"
            });
        }else if(random_1_4 === 3){
            $(".header").css({
                "transition": "all " + shack_time + "s  ",
                "transform": "  translateX(" + shack_px + "px) translateY(" + "-" + shack_px + "px)"
            });
            $(".main").css({
                "transition": "all " + shack_time + "s ",
                "transform": "  translateX(" + shack_px + "px) translateY(" + "-" + shack_px + "px)"
            });
        }else{
            $(".header").css({
                "transition": "all " + shack_time + "s  ",
                "transform": "  translateX(" + "-" + shack_px + "px) translateY(" + "-" + shack_px + "px)"
            })
            $(".main").css({
                "transition": "all " + shack_time + "s ",
                "transform": "  translateX(" + "-" + shack_px + "px) translateY(" + "-" + shack_px + "px)"
            });
        }
        shack_px -- ;
            if( shack_px === 0 ){
                 clearInterval(iid);
            }
    }
    //var iid = setInterval(shake_ani,20);
    function shake_times(){
        window.iid = setInterval(shake_ani,20);
    }


    //------------------------------------------------------------------往下箭頭
    var arrow_ani_time = 650 ;
    var arrow_dowm_img = $(".arrow_dowm img");

    //----------------往上
    function arrow_up() {
        arrow_dowm_img.css({
            "transition": "all " + arrow_ani_time/1000 + "s cubic-bezier(.01,.74,.74,.9) ",
            "transform": "translateY(-10px) rotate(90deg)"
        });
    }
    //----------------往下
    function arrow_down() {
        arrow_dowm_img.css({
            "transition": "all " + arrow_ani_time/1000 + "s cubic-bezier(.51,.24,.94,.53) ",
            "transform": "translateY(10px) rotate(90deg)"
        });
    }
    //---------- 一起動
    function arrow_ani() {
        arrow_up();
        setTimeout(arrow_down, arrow_ani_time);
    }

    //----------滾動觸發
    $(window).scroll(function(){
        var window_scroll_top = $(window).scrollTop();
        console.log("window_scroll_top:" + window_scroll_top );

        if( window_scroll_top < 100){
            //setInterval(arrow_ani,arrow_ani_time*2);
            arrow_dowm_img.addClass("active");
        }else{
            arrow_dowm_img.removeClass("active");
        }
    });

    //----------  一開始觸發

        var window_page_offsetY = window.pageYOffset;
        console.log("window_page_offsetY:" +  window_page_offsetY );

        if( window_page_offsetY < 100 ){
//            setInterval(arrow_ani,arrow_ani_time*2);
            arrow_dowm_img.addClass("active");
        }

//--------------------------------------------------------首頁視差

    $(window).scroll(function(){
        var intel_pro_img= $(".intel_pro_img");
        var pro_intel_txt= $(".pro_intel_txt")
        var window_scroll_top = $(window).scrollTop();
        //window 高度
        var window_height = $(window).height() ;

        //------物件高度
        var header_height = $(".header").height();
//        console.log("header_height:" + header_height);
        var slider_height = $(".slider").height();
//        console.log("slider_height:" + slider_height);
        var arrow_down_height = $(".arrow_dowm").height() + 32 + 20 ;
//        console.log("arrow_down_height:" + arrow_down_height);
        var head_height = $(".head").height() + 50;
//        console.log("head_heiht:" + head_height);
        var min_start_height = header_height + slider_height + arrow_down_height + head_height ;
//        console.log("min_start_height:" + min_start_height);
        var intel_pro_img_height = $(".intel_pro_img").height();
        var pro_intel_area = $(".pro_intel_area");

        var intel_pro_img_offset_top = $(".intel_pro_img").offset().top ;
        var intel_pro_img_start = intel_pro_img_offset_top - head_height - arrow_down_height - slider_height ;
//        console.log("intel_pro_img_start:" + intel_pro_img_start);

        var pro_intel_img_X = window_scroll_top - window_scroll_top* 0.6 - 18 ;
        var pro_intel_txt_X = window_scroll_top - window_scroll_top*1.3  ;

            if( window_scroll_top >= intel_pro_img_start ){
                var intel_pro_img_width = $(".intel_pro_img").width();
                var intel_pro_img_left = $(".intel_pro_img").offset().left ;
                var intel_pro_img_stop = window_width /2 - intel_pro_img_width - 50;

                if(intel_pro_img_left <= intel_pro_img_stop ){
                    intel_pro_img.css({
                        "transform" : "translate3d(" + pro_intel_img_X + "px,0px,0px)"
                    });
                }
                var intel_pro_txt_stop = window_width /2 + 50 ;
                var intel_pro_txt_left = $(".pro_intel_txt").offset().left ;
//                console.log("intel_pro_txt_left:" + intel_pro_txt_left);
                if(intel_pro_txt_left > intel_pro_txt_stop ){
                    pro_intel_txt.css({
                        "transform" : "translate3d(" + pro_intel_txt_X  +  "px,40px,0px)"
                    });
                }

            }

        var intel_pro_img_R = $(".intel_pro_img_R");
        var pro_intel_txt_L = $(".pro_intel_txt_L");
        var pro_intel_txt_L_X2 = window_scroll_top - window_scroll_top*0.72   ;
        var pro_intel_img_L_X2 = window_scroll_top - window_scroll_top* 1.5 + 48;
        var intel_pro_img_R_top = $(".intel_pro_img_R").offset().top ;
        var intel_pro_img_R_height = $(".intel_pro_img_R").height() + 8 + 25 ;
        var intel_pro_img_start2 = intel_pro_img_R_top - intel_pro_img_height - head_height - arrow_down_height - slider_height *0.25 - intel_pro_img_R_height *0.35 ;
//        console.log("pro_intel_img_L_X2: " + pro_intel_img_L_X2);


        if( window_scroll_top >= intel_pro_img_start2) {
            var pro_intel_txt_L_width =  $(".pro_intel_txt_L").width();
            var pro_intel_txt_L_left = $(".pro_intel_txt_L").offset().left + pro_intel_txt_L_width;
            var pro_intel_txt_L_stop = window_width /2 - pro_intel_txt_L_left ;
            var intel_pro_img_R_left = intel_pro_img_R.offset().left;
            var intel_pro_img_R_stop = window_width/2 + 50 ;
//            console.log("pro_intel_txt_L_left"+pro_intel_txt_L_left);
            if( intel_pro_img_R_left >= intel_pro_img_R_stop) {
                intel_pro_img_R.css({
                    "transform": "translate3d(" + pro_intel_img_L_X2 + "px,0px,0px)"
                });
                //            console.log( "pro_intel_txt_L_X:" + pro_intel_txt_L_X );
            }
            if( pro_intel_txt_L_stop >= 50){
                pro_intel_txt_L.css({
                    "transform": "translate3d(" + pro_intel_txt_L_X2  + "px,0px,0px)"
                });
            }
        }

        var sec_hand_pro_img = $(".sec_hand_pro_img") ;
        var sec_hand_txt = $(".sec_hand_txt") ;
        var sec_hand_pro_img_Y = sec_hand_pro_img.offset().top ;
        var sec_hand_pro_img_height = $(".sec_hand_pro_img").height();
        var sec_hand_pro_img_X = window_scroll_top*0.6 - 300 ;
        var sec_hand_txt_X = window_scroll_top - window_scroll_top*0.96  + 300 ;
        var sec_hand_pro_img_start = sec_hand_pro_img_height *0.2 + intel_pro_img_R_height + head_height*2 + intel_pro_img_height + arrow_down_height + slider_height + header_height - window_height ;
//        console.log("sec_hand_pro_img_start:" + sec_hand_pro_img_start);

        if( window_scroll_top >= sec_hand_pro_img_start ) {
            var sec_hand_pro_img_left2 = $(".sec_hand_pro_img").offset().left - $(".sec_hand_pro_img").width() ;
            var sec_hand_pro_img_stop = window_width/2 -50 ;
            if(sec_hand_pro_img_left2 <= sec_hand_pro_img_stop){
                sec_hand_pro_img.css({
                    "transform": "translate3d(" + sec_hand_pro_img_X + "px,0px,0px)"
                });
                sec_hand_txt.css({
                    "transform": "translate3d(" + sec_hand_txt_X  + "px,0px,0px)"
                });
            }
        }

        var sec_hand_pro_text_L = $(".sec_hand_pro_text_L") ;
        var sec_hand_pro_text_L_X =  window_scroll_top *0.3 - 250 ;
        var sec_hand_pro_img_R = $(".sec_hand_pro_img_R") ;
        var sec_hand_pro_img_R_X =  window_scroll_top - window_scroll_top *1.1 + 125   ;
        var sec_hand_pro_img_R_height = sec_hand_pro_img_R.height();
        var sec_hand_pro_img_R_top = sec_hand_pro_img_R.offset().top ;
        var sec_hand_pro_img_R_start = sec_hand_pro_img_start + sec_hand_pro_img_height ;
//        console.log("sec_hand_pro_img_R_start: " + sec_hand_pro_img_R_start);
        if( window_scroll_top > sec_hand_pro_img_R_start) {
            var sec_hand_pro_text_L_left = $(".sec_hand_pro_text_L").offset().left;
            if(sec_hand_pro_text_L_left <= 550) {
                sec_hand_pro_text_L.css({
                    "transform": "translate3d(" + sec_hand_pro_text_L_X + "px,0px,0px)"
                });
                sec_hand_pro_img_R.css({
                    "transform": "translate3d(" + sec_hand_pro_img_R_X + "px,0px,0px)"
                });
            }
        }
        //---------------------------------Pre_order_pro 視差
        var Pre_order_area1 = $(".Pre_order_area1");
        var Pre_order_area1_height = $(".Pre_order_area1").height();
        var Pre_order_area1_start = sec_hand_pro_img_start + sec_hand_pro_img_height + sec_hand_pro_img_R_height + head_height + Pre_order_area1_height *0.2;
//        console.log("Pre_order_pro_1_start: " + Pre_order_pro_1_start);
        var Pre_order_area1_X = window_scroll_top *0.26 - 441 ;
        if( window_scroll_top > Pre_order_area1_start ){
            var Pre_order_area1_left = $(".Pre_order_area1").offset().left;
            //console.log("Pre_order_pro_1_left: " + Pre_order_area1_left);
            if( Pre_order_area1_left < 420 ){
            Pre_order_area1.css(
                "transform" , "translate3d(" + Pre_order_area1_X + "px,0px,0px)");
            }
        }
        var Pre_order_area2 = $(".Pre_order_area2");
        var Pre_order_area2_height = $(".Pre_order_area2").height();
        var Pre_order_area2_start = Pre_order_area1_start + Pre_order_area1_height  ;
//        console.log("Pre_order_area2_start: " + Pre_order_area2_start);

        var Pre_order_area2_X = window_scroll_top - window_scroll_top *1.10 + 120;
        if( window_scroll_top > Pre_order_area2_start ){
            var Pre_order_area2_left =  $(".Pre_order_area2").offset().left;
            if(Pre_order_area2_left >= 124){
            Pre_order_area2.css(
                "transform" , "translate3d(" + Pre_order_area2_X + "px,0px,0px)");
            }
        }
    });
    /*-----------------------------------------------------slider*/
    $('.slider').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        speed: 780,
        fade: false,
        adaptiveHeight: false,
        autoplay: false,
        autoplaySpeed: 4500,
        cssEase: "ease-out",
        prevArrow: '<img src="css/images/index/left.png"  data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button">',
        nextArrow: '<img src="css/images/index/right.png" class="slick-next"></button>',
        focusOnSelect: false,
        easing: "ease-in"
    });

</script>
<?php include __DIR__. '/__page_foot.php' ?>