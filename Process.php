<?php
require __DIR__ . '/__connect_db.php';
$pname = 'Process';


?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta hprop-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="css/reset.css">
	<link rel="stylesheet" type="text/css" href="css/Process.css">
	<?php include __DIR__. "/__page_head.php" ?>
</head>
<body>
	<?php include __DIR__ . '/__page_header.php' ?>
	<div class="process_box">
		<p class="wellcome_header">二手拍賣流程</p>
		<div class="title">
			<div class="content1">
				<img class="process_img" src="css/images/Process/Nagarehodo.png">
			</div>
			<div class='content2'>
				<p class="wellcome_header">二手介紹</p>			
				<p class="wellcome_content">「歡迎光臨」Fantaroid二手商品館！　<br>這裡的都是進新品的二手貨 ，還有琳瑯滿目的商品，及忙著擦拭商品、整理上架的專業人員，而我們有專人為您把關商品品質。<br>這理也是個二手的販賣平台 ，可以找到你最喜歡的二頭身公仔喔！<br>喜愛Q板二頭身的您，是否常想擁有更多二頭身公仔？<br>或是您身邊也有您收藏已久的公仔呢？<br>歡迎來到Fantaroid，趕快加入【Fantaroid會員】，告訴我們您對公仔的喜愛及熱忱吧！</p>
			</div>	
		</div><!-- title -->
		<div class="title_two">
			<p class="wellcome_header">二手拍賣詳細規章</p>
			<!-- <hr> -->
				<div class="process-inner">			
					<div class="process-header">
					</div>
					<div class="notice rules">
						<h2 class="pro">一、認知與接受條款</h2>
						<ol class="process_all">
						<li>1.Fantaroid實業股份有限公司(以下簡稱「Fantaroid」)係依據本服務條款提供Fantaroid服務 (以下簡稱「本服務」)。當會員完成Fantaroid之會員註冊手續、或開始使用本服務時，即表示已閱讀、瞭解並同意接受本服務條款之所有內容，並完全接受本服務現有與未來衍生的服務項目及內容。
						</li><li>2.	Fantaroid公司有權於任何時間修改或變更本服務條款之內容，修改後的服務條款內容將公佈網站上，Fantaroid將不會個別通知會員，建議會員隨時注意該等修改或變更。會員於任何修改或變更後繼續使用本服務時，視為會員已閱讀、瞭解並同意接受該等修改或變更。若不同意上述的服務條款修訂或更新方式，或不接受本服務條款的其他任一約定，會員應立即停止使用本服務。
						
						</li><li>3.	會員及Fantaroid雙方同意使用本服務之所有內容包括意思表示等，以電子文件作為表示方式。
						</li></ol>
						
						<h2 class="pro">二、會員的註冊義務</h2>
						<ol class="process_all">
						<p>為了能使用本服務，會員同意以下事項：</p>
						
						<li>1.	依本服務註冊表之提示提供會員本人正確、最新的資料，且不得以第三人之名義註冊為會員。每位會員僅能註冊登錄一個帳號，不可重覆註冊登錄。
						</li><li>2.	即時維持並更新會員個人資料，確保其正確性，以獲取最佳之服務。
						</li><li>3.	若會員提供任何錯誤或不實的資料、或未按指示提供資料、或欠缺必要之資料、或有重覆註冊帳號等情事時，Fantaroid有權不經事先通知，逕行暫停或終止會員的帳號，並拒絕會員使用本服務之全部或一部分。
						</li></ol>
						
						<h2 class="pro">三、Fantaroid隱私權政策</h2>
						<p class="process_all">為了確保會員之個人資料、隱私權及消費者權益之保護，於交易過程中將使用會員之個人資料，謹依個人資料保護法第8條規定告知以下事項：</p>
						<ol>
						<li>1.	蒐集之目的：
						蒐集之目的在於進行行銷業務、消費者、客戶管理與服務、網路購物及其他電子商務服務及調查、統計與研究分析。Fantaroid將藉由加入會員之過程或進行交易之過程來蒐集個人資料。
						</li><li>2.	蒐集之個人資料類別：
						    <ol class="process_all">
						    <li>(1) 辨識個人者：如會員姓名、地址、電話、手機、電子郵件等資訊。
						    </li><li>(2) 辨識財務者：如信用卡或金融機構帳戶資訊。
						    </li><li>(3) 個人描述：例如：性別、出生年月等。
						    </li></ol>
						</li><li>3.	會員就個人資料之權利：
						<p>Fantaroid所蒐集個人資料之當事人，依個人資料保護法得對Fantaroid行使以下權利：</p>
						    <ol class="process_all">
						    <li>(1) 查詢或請求閱覽。
						    </li><li>(2) 請求製給複製本。
						    </li><li>(3) 請求補充或更正。
						    </li><li>(4) 請求停止蒐集、處理或利用。
						    </li><li>(5) 請求刪除。<br>會員如欲行使上述權利，可與Fantaroid客戶服務專線：0800-031-720連絡進行申請。
						    </li></ol>
						
						</li><li>4.	請注意！如拒絕提供加入會員所需必要之資料，將可能導致無法享受完整服務或完全無法使用該項服務。
						</li></ol>  
						
						
						<h2 class="pro">四、會員帳號、密碼及安全</h2>
						<ol class="process_all">
						<li>1.	完成本服務的登記程序之後，會員將取得一個特定之密碼及會員帳號，維持密碼及帳號之機密安全，是會員的責任。任何依照規定方法輸入會員帳號及密碼與登入資料一致時，無論是否由本人親自輸入，均將推定為會員本人所使用，利用該密碼及帳號所進行的一切行動，會員本人應負完全責任。
						</li><li>2.	會員同意以下事項：
						    <ol class="process_all">
						    <li>(1)會員的密碼或帳號遭到盜用或有其他任何安全問題發生時，會員將立即通知Fantaroid。
						    </li><li>(2)每次連線完畢，均結束會員的帳號使用。
						    </li><li>(3)會員的帳號、密碼及會員權益均僅供會員個人使用及享有，不得轉借、轉讓他人或與他人合用。
						    </li><li>(4)帳號及密碼遭盜用、不當使用或其他Fantaroid無法辯識是否為本人親自使用之情況時，對此所致之損害，除證明係因可歸責於Fantaroid之事由所致，Fantaroid將不負任何責任。
						    </li><li>(5)Fantaroid若知悉會員之帳號密碼確係遭他人冒用時，將立即暫停該帳號之使用(含該帳號所生交易之處理)。
						    </li></ol>
						</li></ol>
						<ol class="process_all">
						<h2 class="pro">五、使用者的守法義務及承諾</h2>
						<p>會員承諾絕不為任何非法目的或以任何非法方式使用本服務，並承諾遵守中華民國相關法規及一切使用網際網路之國際慣例。會員若係中華民國以外之使用者，並同意遵守所屬國家或地域之法令。會員同意並保證不得利用本服務從事侵害他人權益或違法之行為，包括但不限於：</p></ol>
						<ol class="process_all">
						<li>1.	公布或傳送任何誹謗、侮辱、具威脅性、攻擊性、不雅、猥褻、不實、違反公共秩序或善良風俗或其他不法之文字、圖片或任何形式的檔案。
						</li><li>2.	侵害或毀損Fantaroid或他人名譽、隱私權、營業秘密、商標權、著作權、專利權、其他智慧財產權及其他權利。
						</li><li>3.	違反依法律或契約所應負之保密義務。
						</li><li>4.	冒用他人名義使用本服務。
						</li><li>5.	傳輸或散佈電腦病毒。
						</li><li>6.	從事未經Fantaroid事前授權的商業行為。
						</li><li>7.	刊載、傳輸、發送垃圾郵件、連鎖信、違法或未經Fantaroid許可之多層次傳銷訊息及廣告等；或儲存任何侵害他人智慧財產權或違反法令之資料。
						</li><li>8.	對本服務其他用戶或第三人產生困擾、不悅或違反一般網路禮節致生反感之行為。
						</li><li>9.	其他不符本服務所提供的使用目的之行為或Fantaroid有正當理由認為不適當之行為。
						</li></ol>
						
						<h2 class="pro">六、服務內容之變更與電子報及EDM發送</h2>
						<ol class="process_all">
						<li>1.	會員同意Fantaroid所提供本服務之範圍，Fantaroid均得視業務需要及實際情形，增減、變更或終止相關服務的項目或內容，且無需個別通知會員。
						</li><li>2.	會員同意Fantaroid得依實際執行情形，增加、修改或終止相關活動，並選擇最適方式告知會員。
						</li><li>3.	會員同意Fantaroid得不定期發送電子報或商品訊息(EDM)至會員所登錄的電子信箱帳號。當會員收到訊息後表示拒絕接受行銷時，Fantaroid將停止繼續發送行銷訊息。
						</li></ol>
						
						<h2 class="pro">七、服務之停止、中斷</h2><ol class="process_all">
						<p>Fantaroid將依一般合理之技術及方式，維持系統及服務之正常運作。但於以下各項情況時，Fantaroid有權可以停止、中斷提供本服務：</p>
						
						<li>1.	Fantaroid網站電子通信設備進行必要之保養及施工時。
						</li><li>2.	發生突發性之電子通信設備故障時。
						</li><li>3.	Fantaroid網站申請之電子通信服務被停止，無法提供服務時。
						</li><li>4.	由於天災等不可抗力之因素或其他不可歸責於Fantaroid致使Fantaroid網站無法提供服務時。
						</li></ol>
						
						<h2 class="pro">八、交易行為</h2>
						<ol class="process_all">
						<li>1.	會員使用本服務進行交易時，應依據Fantaroid所提供之確認商品數量及價格機制進行。
						</li><li>2.	會員同意使用本服務訂購產品時，於Fantaroid通知確認交易成立前，Fantaroid仍保有不接受訂單或取消出貨之權利。會員向Fantaroid發出訂購通知後，系統將自動發出接受通知。若因訂單內容之標的商品或服務，其交易條件(包括但不限於規格、內容說明、圖片)有誤時，將電話聯繫消費者。
						</li><li>3.	會員若於使用本服務訂購產品後無故退換貨、取消訂單、或有任何Fantaroid認為不適當而造成Fantaroid作業上之困擾或損害之行為，Fantaroid將可視情況採取必要註記等措施。
						</li><li>4.	若會員訂購之產品屬於以下情形：
						    <ol class="process_all">
						    <li>(1)預購類商品
						    </li><li>(2)商品頁顯示無庫存
						    </li><li>(3)須向供應商調貨
						    </li><li>(4)因商品交易特性之故，若商品缺絕、或廠商因故無法順利供貨導致訂單無法成立時，Fantaroid將以最適方式(以電子郵件為主，再輔以電話、郵遞或傳真等)告知。
						    </li></ol>
						</li><li>5.	會員使用本服務進行交易時，得依照消費者保護法之規定行使權利。因會員之交易行為而對本服務條款產生疑義時，應為有利於消費者之解釋。
						</li></ol>
						
						<h2 class="pro">九、責任之限制與排除</h2>
						<ol class="process_all">
						<li>1.	本服務之網頁、伺服器、網域等所提供之各項功能，均依該功能當時之現況提供使用，Fantaroid對於其效能、速度、完整性、可靠性、安全性、正確性等，皆不負擔任何明示或默示之擔保責任。
						</li><li>2.	Fantaroid並不保證本服務之網頁、伺服器、網域等所傳送的電子郵件或其內容不會含有電腦病毒等有害物；亦不保證郵件、檔案或資料之傳輸儲存均正確無誤不會斷線和出錯等，因各該郵件、檔案或資料傳送或儲存失敗、遺失或錯誤等所致之損害，Fantaroid不負賠償責任。
						</li></ol>
						
						<h2 class="pro">十、智慧財產權的保護</h2>
						<ol class="process_all">
						<li>1.	Fantaroid所使用之軟體或程式、網站上所有內容，包括但不限於著作、圖片、檔案、資訊、資料、網站架構、網站畫面的安排、網頁設計，均由Fantaroid或其他權利人依法擁有其智慧財產權，包括但不限於商標權、專利權、著作權、營業秘密與專有技術等。任何人不得逕自使用、修改、重製、公開播送、改作、散布、發行、公開發表、進行還原工程、解編或反向組譯。若會員欲引用或轉載前述軟體、程式或網站內容，必須依法取得Fantaroid或其他權利人的事前書面同意。
						</li><li>2.	在尊重他人智慧財產權之原則下，會員同意在使用Fantaroid之服務時，不作侵害他人智慧財產權之行為。
						</li><li>3.	若會員有涉及侵權之情事，Fantaroid可暫停全部或部份之服務，或逕自以取消會員帳號之方式處理。
						</li></ol>
						
						<h2 class="pro">十一、會員對Fantaroid之授權</h2>
						<ol class="process_all">
						<p>對於會員上載、傳送、輸入或提供之資料，會員同意Fantaroid網站得於合理之範圍內蒐集、處理、保存、傳遞及使用該等資料，以提供使用者其他資訊或服務、或作成會員統計資料、或進行關於網路行為之調查或研究，或為任何之合法使用。若會員無合法權利得授權他人使用、修改、重製、公開播送、改作、散布、發行、公開發表某資料，請勿擅自將該資料上載、傳送、輸入或提供至Fantaroid。<br>
						任何資料一經會員上載、傳送、輸入或提供至Fantaroid時，視為會員已允許Fantaroid無條件使用、修改、重製、公開播送、改作、散布、發行、公開發表該等資料，並得將前述權利轉授權他人，會員對此絕無異議。會員並應保證擁有合法權利，得授權Fantaroid使用、修改、重製、公開播送、改作、散布、發行、公開發表、轉授權該等資料，不致侵害任何第三人主張侵害其智慧財產權等權利，否則會員應對Fantaroid負損害賠償責任（包括但不限於訴訟費用及律師費用等）。</p></ol>
					</div>
				</div>
				<div class="process_buttom">
					<a class="process_back" href="secondhand.php">回到二手商品頁</a>
					<a class="process_willsale" href="process_rule.php">我要販賣</a>
				</div>
		</div>

	</div><!-- process_box -->
<?php include __DIR__. "/__page_foot.php" ?>
</body>
</html>