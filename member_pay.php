<?php
require __DIR__ . '/__connect_db.php';
$pname = 'member_pay';
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta hprop-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="css/reset.css">
	<link rel="stylesheet" type="text/css" href="css/member_pa.css">
<?php include __DIR__. "/__page_head.php" ?>
<title>會員購物明細付款方式</title>

</head>
<body>
<?php include __DIR__. '/__page_header.php' ?>
<div class="member_box">
<p class="title_word">取貨方式</p>
<div class="container">
	<form>
	<input type="radio" name="pick_up" value="">超商取貨
	<br>
	<input type="radio" name="pick_up" value="">宅配
	<br>
	<input type="radio" name="pick_up" value="">店取
	</form> 
</div>
<p class="title_word">付款方式</p>

<div class="container">
	<form>
	<input type="radio" name="payment" value="">超商付款
	<br>
	<input type="radio" name="payment" value="">信用卡
	<br>
	<input type="radio" name="payment" value="">店取付款
	</form> 
</div>
<p class="title_word">使用購物金/優惠券</p>
<div class="container">
	<form>
	<input type="radio" name="coupon" value="" >優惠券<input type="text" name="name" value="" placeholder="優惠卷代碼"><br>
	<br>
	<input type="radio" name="coupon" value="">購物金
	</form> 
</div>
<p class="title_word">訂購人資訊</p>
<div class="container">
    <?php if(isset($_SESSION['user'])): ?>
	<form  class="member_form" action="/action_page.php">
	姓名　　<input type="text" name="name" value="<?= $_SESSION['user']['name'] ?>"><br>
	電話　　<input type="text" name="phone" value="<?= $_SESSION['user']['mobile'] ?>"><br>
	<span class="airal">電子郵件</span><input type="text" name="name" value="<?= $_SESSION['user']['email'] ?>"><br>
	收件地址<input type="text" name="phone" value=""><br>
	宅配時間<input type="text" name="name" value=""><br>
	
	</form>
    <?php endif; ?>
</div>
<div class="member_buttom">
	<a href="cart_list.php"><div class="member_right">上一步</div></a>
	<a href="cart_confirm.php"><div class="member_left ">確認</div></a>
</div>	
</div>
<?php include __DIR__. "/__page_foot.php" ?>
	</body>
</html>